/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Related_products : Codable {
	var id = 0
	var name = ""
	var description = ""
	var price = 0
	var product_image = ""
	var discounted_price = 0
	var share_link = ""
	var user_favorite = 0
	var rate = ""

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case description = "description"
		case price = "price"
		case product_image = "product_image"
		case discounted_price = "discounted_price"
		case share_link = "share_link"
		case user_favorite = "user_favorite"
		case rate = "rate"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
		name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
		description = try values.decodeIfPresent(String.self, forKey: .description) ?? ""
		price = try values.decodeIfPresent(Int.self, forKey: .price) ?? 0
		product_image = try values.decodeIfPresent(String.self, forKey: .product_image) ?? ""
		discounted_price = try values.decodeIfPresent(Int.self, forKey: .discounted_price) ?? 0
		share_link = try values.decodeIfPresent(String.self, forKey: .share_link) ?? ""
		user_favorite = try values.decodeIfPresent(Int.self, forKey: .user_favorite) ?? 0
		rate = try values.decodeIfPresent(String.self, forKey: .rate) ?? ""
	}

    init(){
        id = 0
        name = ""
        description = ""
        price = 0
        product_image = ""
        discounted_price = 0
        share_link = ""
        user_favorite = 0
        rate = ""

    }
}
