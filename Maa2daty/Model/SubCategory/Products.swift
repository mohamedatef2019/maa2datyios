/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Products : Codable {
	var current_page = 0
	var singleProduct : [SingleProduct] = []
	var first_page_url = ""
	var from = 0
	var last_page = 0
	var last_page_url = ""
	var next_page_url = ""
	var per_page = 0
	var prev_page_url = ""
	var to = 0
	var total = 0

	enum CodingKeys: String, CodingKey {

		case current_page = "current_page"
		case singleProduct = "data"
		case first_page_url = "first_page_url"
		case from = "from"
		case last_page = "last_page"
		case last_page_url = "last_page_url"
		case next_page_url = "next_page_url"
		case per_page = "per_page"
		case prev_page_url = "prev_page_url"
		case to = "to"
		case total = "total"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        current_page = try values.decodeIfPresent(Int.self, forKey: .current_page) ?? 0
		singleProduct = try values.decodeIfPresent([SingleProduct].self, forKey: .singleProduct) ?? []
		first_page_url = try values.decodeIfPresent(String.self, forKey: .first_page_url) ?? ""
		from = try values.decodeIfPresent(Int.self, forKey: .from) ?? 0
		last_page = try values.decodeIfPresent(Int.self, forKey: .last_page) ?? 0
		last_page_url = try values.decodeIfPresent(String.self, forKey: .last_page_url) ?? ""
		next_page_url = try values.decodeIfPresent(String.self, forKey: .next_page_url) ?? ""
		per_page = try values.decodeIfPresent(Int.self, forKey: .per_page) ?? 0
		prev_page_url = try values.decodeIfPresent(String.self, forKey: .prev_page_url) ?? ""
		to = try values.decodeIfPresent(Int.self, forKey: .to) ?? 0
		total = try values.decodeIfPresent(Int.self, forKey: .total) ?? 0
	}
    
    init(){
        current_page = 0
        singleProduct = []
        first_page_url = ""
        from = 0
        last_page = 0
        last_page_url = ""
        next_page_url = ""
        per_page = 0
        prev_page_url = ""
        to = 0
        total = 0
    }

}
