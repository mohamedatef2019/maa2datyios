//
//  subCategoryDetails.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation
struct SingleProduct : Codable {
    var id = 0
    var name = ""
    var description = ""
    var price = 0
    var discounted_price = 0
    var product_image = ""
    var share_link = ""
    var user_favorite = 0
    var rate = ""
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case description = "description"
        case price = "price"
        case discounted_price = "discounted_price"
        case product_image = "product_image"
        case share_link = "share_link"
        case user_favorite = "user_favorite"
        case rate = "rate"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        description = try values.decodeIfPresent(String.self, forKey: .description) ?? ""
        price = try values.decodeIfPresent(Int.self, forKey: .price) ?? 0
        discounted_price = try values.decodeIfPresent(Int.self, forKey: .discounted_price) ?? 0
        product_image = try values.decodeIfPresent(String.self, forKey: .product_image) ?? ""
        share_link = try values.decodeIfPresent(String.self, forKey: .share_link) ?? ""
        user_favorite = try values.decodeIfPresent(Int.self, forKey: .user_favorite) ?? 0
        rate = try values.decodeIfPresent(String.self, forKey: .rate) ?? ""
    }
    
    
    
}
