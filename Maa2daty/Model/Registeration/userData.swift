/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct userData : Codable {
	var id = 0
	var first_name = ""
	var last_name = ""
	var phone = ""
	var email = ""
	var user_image = ""
	var jwt_token = ""
	var user_status = 0
	var location = ""
	var lat = ""
	var lng = ""

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case first_name = "first_name"
		case last_name = "last_name"
		case phone = "phone"
		case email = "email"
		case user_image = "user_image"
		case jwt_token = "jwt_token"
		case user_status = "user_status"
		case location = "location"
		case lat = "lat"
		case lng = "lng"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name) ?? ""
		last_name = try values.decodeIfPresent(String.self, forKey: .last_name) ?? ""
		phone = try values.decodeIfPresent(String.self, forKey: .phone) ?? ""
		email = try values.decodeIfPresent(String.self, forKey: .email) ?? ""
		user_image = try values.decodeIfPresent(String.self, forKey: .user_image) ?? ""
		jwt_token = try values.decodeIfPresent(String.self, forKey: .jwt_token) ?? ""
		user_status = try values.decodeIfPresent(Int.self, forKey: .user_status) ?? 0
		location = try values.decodeIfPresent(String.self, forKey: .location) ?? ""
		lat = try values.decodeIfPresent(String.self, forKey: .lat) ?? ""
		lng = try values.decodeIfPresent(String.self, forKey: .lng) ?? ""
	}

    init(){
        id = 0
        first_name = ""
        last_name = ""
        phone = ""
        email = ""
        user_image = ""
        jwt_token = ""
        user_status = 0
        location = ""
        lat = ""
        lng = ""
    }
}
