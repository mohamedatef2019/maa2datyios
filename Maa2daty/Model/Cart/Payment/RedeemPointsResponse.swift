//
//  RedeemPointsResponse.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/26/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

struct RedeemPointsResponse : Codable {
    var status = 0
    var message = ""
    var data : RedeemPointsData = RedeemPointsData()
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status) ?? 0
        message = try values.decodeIfPresent(String.self, forKey: .message) ?? ""
        data = try values.decodeIfPresent(RedeemPointsData.self, forKey: .data) ?? RedeemPointsData()
    }
    
}
struct RedeemPointsData : Codable {
    var total_fees = 0
    
    enum CodingKeys: String, CodingKey {
        
        case total_fees = "total_fees"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total_fees = try values.decodeIfPresent(Int.self, forKey: .total_fees) ?? 0
    }
    
    init(){
        total_fees = 0
    }
    
}


