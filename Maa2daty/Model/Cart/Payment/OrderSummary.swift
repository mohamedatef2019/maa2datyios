/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct OrderSummary : Codable {
	var order_id = 0
	var shipping = "0"
	var user_points = ""
	var address_list : [Address_list] = []

	enum CodingKeys: String, CodingKey {

		case order_id = "order_id"
		case shipping = "shipping"
		case user_points = "user_points"
		case address_list = "address_list"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		order_id = try values.decodeIfPresent(Int.self, forKey: .order_id) ?? 0
		shipping = try values.decodeIfPresent(String.self, forKey: .shipping) ?? "0"
		user_points = try values.decodeIfPresent(String.self, forKey: .user_points) ?? ""
		address_list = try values.decodeIfPresent([Address_list].self, forKey: .address_list) ?? []
	}
    
    init() {
        order_id = 0
        shipping = ""
        user_points = ""
        address_list = []
    }

}
