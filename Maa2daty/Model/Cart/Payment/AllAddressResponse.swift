//
//  File.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/26/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

struct AllAddressResponse : Codable {
    var status = 0
    var message = ""
    var data : [Address_list] = []
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status) ?? 0
        message = try values.decodeIfPresent(String.self, forKey: .message) ?? ""
        data = try values.decodeIfPresent([Address_list].self, forKey: .data) ?? []
    }
    
}

