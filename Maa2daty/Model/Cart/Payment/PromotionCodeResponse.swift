//
//  PromotionCodeResponse.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/26/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation
struct PromotionCodeResponse : Codable {
    var status = 0
    var message = ""
    var data : Discounted_Price = Discounted_Price()
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status) ?? 0
        message = try values.decodeIfPresent(String.self, forKey: .message) ?? ""
        data = try values.decodeIfPresent(Discounted_Price.self, forKey: .data) ?? Discounted_Price()
    }
    
}
struct PromotionCodeData : Codable {
    var new_subtotal = ""
    var total_fees = ""
    
    enum CodingKeys: String, CodingKey {
        
        case new_subtotal = "new_subtotal"
        case total_fees = "total_fees"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        new_subtotal = try values.decodeIfPresent(String.self, forKey: .new_subtotal) ?? ""
        total_fees = try values.decodeIfPresent(String.self, forKey: .total_fees) ?? ""
    }
    
    init(){
        new_subtotal = ""
        total_fees = ""
    }
    
}
struct Discounted_Price : Codable {
    var discounted_price = PromotionCodeData()
    
    enum CodingKeys: String, CodingKey {
        
        case discounted_price = "discounted_price"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        discounted_price = try values.decodeIfPresent(PromotionCodeData.self, forKey: .discounted_price) ?? PromotionCodeData()
    }
    
    init(){
        discounted_price = PromotionCodeData()
    }
    
}
