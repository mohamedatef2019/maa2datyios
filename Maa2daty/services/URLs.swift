//
//  URLs.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class URLs{
    static let url    = "http://maedaty.my-staff.net"
    static let base   = url + "/api"
    
    
    //MARK:- user
    static let user        = base    + "/user"
    static let review      = user    + "/review"
    static let fav         = user    + "/favorite"
    static let profile     = user    + "/profile"
    static let addNewAddress = user  + "/address"
    static let editAddress = user    + "/address/edit"
    static let editProfile = profile + "/edit"
    static let socialLogin = base    + "/social_login"
    
    static let Category = base + "/category"
    static let Products = base + "/product/"
    static let allProducts = Products + "list/"
    
    static let setRate = review + "/add"
    static let search = base + "/product/search?string="
    
    //MARK:- cart
    static let cart       = user    + "/cart/"
    static let addToCart  = cart + "add_or_update"
    static let deleteCart = cart + "delete"
    
    //MARK:- Favourite
    static let favouriteProduct =  fav + "/add_or_remove"
    static let AllFavourite     =  user + "/favorite/"
    
    //MARK:- Offers
    static let offers = Products + "offers/"
    
    //MARK:- Shops
    static let shops        = base + "/shop"
    static let allShops     = shops + "/list"
    static let categoryShop = shops + "/categories/"
    static let productsShop = Products + "shop/"
    
    static let termsAndConditions = url + "/terms_and_conditions"
    static let aboutUs            = url + "/about_us"
    
    //MARK:- RegisterationCycle
    static let login          = base    + "/login"
    static let forget         = base    + "/forgot_password"
    static let activate       = base    + "/activate"
    static let changePassword = profile + "/change_password"
    static let Register       = base    + "/signup"
    static let LogoutOut      = profile + "/logout"
    
    //MARK:- suggest
    static let addSuggest = base + "/suggestion/add"
    
    //MARK:- Orders
//    static let order = base + "/order"
    static let orders = base + "/order"
    static let allOrder = orders + "/user/list/"
    static let orderSummary = orders + "/user/"
    
    //MARK:- Notification
    static let notification = base + "/notification/list/"
    
    //MARK:- Messages
    static let message     = base + "/message"
    static let allMessage  = message + "/list/"
    static let sendMessage = message + "/send"
    
    //MARK:- points
    static let redeemPoints = base + "/point/redeem"
    static let promoCode    = base + "/promo/apply"
}
