//
//  ApiManager.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation
import Alamofire

class APIManager {
    
    //MARK: - Singlton
    class var sharedInstance : APIManager {
        struct Singlton {
            static let instance = APIManager()
        }
        
        return Singlton.instance
    }
    
    
    // MARK: - general request
    func getRequest(_ url : String ,completionHandler :@escaping (DataResponse<Any>) -> Void) {
        _ = Alamofire.request(url,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            completionHandler(response)
        }
    }
    
    func postRequest(_ url : String ,Parameters : [String: Any],completionHandler :@escaping (DataResponse<Any>) -> Void) {
        _ = Alamofire.request(url,method: .post, parameters: Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            completionHandler(response)
        }
    }
    func postRequestWithHeader(_ url : String , header: [String: String] ,Parameters : [String: Any],completionHandler :@escaping (DataResponse<Any>) -> Void) {
        
        _ = Alamofire.request(url,method: .post, parameters: Parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            completionHandler(response)
        }
    }
    
    func getRequestWithLanguage(_ url : String, headers: [String: String] ,completionHandler :@escaping (DataResponse<Any>) -> Void) {
//        let headers = ["language":SharedHandler.getLanguage()]
        _ = Alamofire.request(url,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            completionHandler(response)
        }
    }
}
