//
//  RegisterVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class RegisterVC: UIViewController {

    
    //MARK:- Variable
    var lat = "0.0"
    var lang = "0.0"
    var isSelectImage = false
    var selectedImages = UIImage()
    var presenter: RegisterationVCPresenter?
    let selectedImage = UIImagePickerController()
    
    
    //MARK:- Outlets
    @IBOutlet weak var btn_Terms: UIButton!
    @IBOutlet weak var tf_Email: UITextField!
    @IBOutlet weak var tf_Phone: UITextField!
    @IBOutlet weak var tf_Password: UITextField!
    @IBOutlet weak var tf_LastName: UITextField!
    @IBOutlet weak var tf_Location: UITextField!
    @IBOutlet weak var tf_FirstName: UITextField!
    @IBOutlet weak var img_ProfileImage: RoundImage!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    
    //MARK:- Actions
    @IBAction func termsAction(_ sender: Any) {
        presenter?.navigateTermsVC()
    }
    @IBAction func readTermsActions(_ sender: Any) {
        if btn_Terms.tag == 0{
            btn_Terms.tag = 1
            btn_Terms.setImage(UIImage(named: "checked-box"), for: UIControl.State.normal)
        }else{
            btn_Terms.tag = 0
            btn_Terms.setImage(UIImage(named: "Untitled-1"), for: UIControl.State.normal)
        }
    }
    
    @IBAction func LoginAction(_ sender: Any) {
        if btn_Terms.tag == 1{
            presenter?.validationsOnFields(firstName: tf_FirstName, lastName: tf_LastName, phone: tf_Phone, mail: tf_Email, password: tf_Password, location: tf_Location, lat: lat, lang: lang, isSelectPhoto: isSelectImage, selectedImage: selectedImages)
        }else{
            SharedHandler.showMsg(message: "Please, Read Terms And Conditions".localized, image: "warning", ViewController: self)
        }
    }
    
    @IBAction func selectProfileImageAction(_ sender: Any) {
        setupSelectedImage()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(refreshLocation), name: NSNotification.Name(rawValue: "refreshLocation"), object: nil)
    }

}
