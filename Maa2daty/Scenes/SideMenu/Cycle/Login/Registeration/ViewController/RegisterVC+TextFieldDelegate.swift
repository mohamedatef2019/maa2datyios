//
//  RegisterVC+TextFieldDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation


extension RegisterVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.endEditing(true)
        presenter?.navigateMapVC()
    }
    
    @objc func refreshLocation(notification: Notification){
        let data = notification.object as! mapVC
        self.tf_Location.text = data.geoLocation
        self.lat = data.latitude
        self.lang = data.longitude
    }
    
    @objc func Confirmation(){
        btn_Terms.tag = 1
        btn_Terms.setImage(UIImage(named: "checked-box"), for: UIControl.State.normal)
    }
    @objc func cancel(){
        btn_Terms.tag = 0
        btn_Terms.setImage(UIImage(named: "Untitled-1"), for: UIControl.State.normal)
    }
}
