//
//  File.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import GoogleMaps

class mapVC: UIViewController {
    
    //MARK:- Variable
    var latitude: String = SharedHandler.getlat()
    var longitude: String = SharedHandler.getlng()
    var geoLocation = ""
    let marker = GMSMarker()
    var locationManager = CLLocationManager()
    
    //MARK:- outlets
    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SharedHandler.isLogged(){
            googleMap()
        }else{
            self.mapView.isMyLocationEnabled = true
            self.locationManager.delegate = self
            self.locationManager.startUpdatingLocation()
        }
        marker.isDraggable = true
        let barBtn = UIBarButtonItem(title: "Done".localized, style: UIBarButtonItem.Style.done, target: self, action: #selector(popUp))
        self.navigationItem.rightBarButtonItem = barBtn
    }
    
    @objc func popUp(){
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "refreshLocation"), object: self)
        self.navigationController?.popViewController(animated: true)
    }
    
    func googleMap(){
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude)!, longitude: CLLocationDegrees(longitude)!, zoom: 5.5)
        mapView.animate(to: camera)
        
        marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude)!, longitude: CLLocationDegrees(longitude)!)
        marker.title = " "
        marker.map = mapView
        marker.snippet = " "
    }
    
}
extension mapVC: CLLocationManagerDelegate , GMSMapViewDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        latitude = "\((location?.coordinate.latitude)!)"
        longitude = "\((location?.coordinate.longitude)!)"
        self.mapView.animate(to: camera)
        googleMap()
        getAddressFromLatLon(pdblLatitude: latitude, withLongitude: longitude)
        self.locationManager.stopUpdatingLocation()
    }
    
    func mapView (_ mapView: GMSMapView, didEndDragging didEndDraggingMarker: GMSMarker) {
        latitude = String(marker.position.latitude)
        longitude = String(marker.position.longitude)
        getAddressFromLatLon(pdblLatitude: latitude, withLongitude: longitude)
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        //        indicator.startAnimating()
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat = Double("\(pdblLatitude)")!
        //21.228124
        let long = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        
        center.latitude = lat
        center.longitude = long
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    var addressString : String = ""
                    
                    if pm.subThoroughfare != nil {
                        addressString = addressString + pm.subThoroughfare! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + " "
                    }
                    self.geoLocation = addressString
                }
        })
        
    }
}

