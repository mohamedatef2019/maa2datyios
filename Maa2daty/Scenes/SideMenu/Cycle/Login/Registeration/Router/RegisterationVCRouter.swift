//
//  RegisterationVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class RegisterationVCRouter{
    class func createRegisterVC() -> UIViewController {
        
        let RegisterVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterVC")
        if let view = RegisterVC as? RegisterationVCView {
            let interactor = RegisterationVCInteracttor()
            let router = RegisterationVCRouter()
            let presenter = RegisterationVCPresenter(view: view, interactor: interactor, router: router)
            view.presenter = presenter
        }
        return RegisterVC
    }
    
    func navigateVerifyCodeVC(from view: RegisterationVCView?, phone: String, isRegister: Bool) {
        let viewww = VerificationCodeVCRouter.createVerificationCodeVC(phone: phone, isRegister: isRegister)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(viewww, animated: true)
        }
    }
    
    func navigateMapVC(from view: RegisterationVCView?) {
        let viewww = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapVC")
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(viewww, animated: true)
        }
    }
    
    func navigateTermsVC(from view: RegisterationVCView?) {
        let viewww = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "termsVC")
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(viewww, animated: true)
        }
    }
}
