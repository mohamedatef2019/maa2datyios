//
//  RegisterationVCPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol RegisterationVCView: class {
    var presenter: RegisterationVCPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchingDataSuccess()
    func showMsg(message: String, image: String)
}
class RegisterationVCPresenter{
    private weak var view: RegisterationVCView?
    private let interactor: RegisterationVCInteracttor
    private let router: RegisterationVCRouter
    
    init(view: RegisterationVCView?, interactor: RegisterationVCInteracttor, router: RegisterationVCRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func validationsOnFields(firstName: UITextField, lastName: UITextField, phone: UITextField, mail: UITextField, password: UITextField, location: UITextField, lat: String, lang: String, isSelectPhoto: Bool, selectedImage: UIImage){
        if SharedHandler.isValidAction(textFeilds: [firstName, lastName, phone, mail, password, location]){
            if phone.text!.isPhone(){
                if mail.text!.isValidEmail(testStr: mail.text!){
                    
                    if isSelectPhoto{
                        let param = [
                            "first_name":firstName.text!,
                            "last_name":lastName.text!,
                            "phone":phone.text!,
                            "email":mail.text!,
                            "password":password.text!,
                            "location":location.text!,
                            "lat":lat,
                            "lng":lang,
                            "from":"ios"
                        ]
                        print(param)
                        prepareForRegisteration(param: param, image: selectedImage)
                    }else{
                        self.view?.showMsg(message: "Please, Select Your Profile Photo".localized, image: "warning")
                    }
                    
                }else{
                    self.view?.showMsg(message: "Please, Enter Valid Email".localized, image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Enter Valid Phone Number".localized, image: "warning")
            }
        }else{
            self.view?.showMsg(message: "All Fields is required".localized, image: "warning")
        }
    }
    
    private func prepareForRegisteration(param: [String: String], image: UIImage){
        view?.showIndicator()
        interactor.RegisterRequest(selectedImage: image, param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    SharedHandler.settingUserDefauls(userData: res!.data)
                    self.view?.fetchingDataSuccess()
                    self.view?.showMsg(message: res?.message ?? "", image: "like")
                }else{
                    self.view?.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    func navigateToVerifyCode(phone: String){
        router.navigateVerifyCodeVC(from: view, phone: phone, isRegister: true)
    }
    
    func navigateMapVC(){
        router.navigateMapVC(from: view)
    }
    
    func navigateTermsVC(){
        router.navigateTermsVC(from: view)
    }
}
