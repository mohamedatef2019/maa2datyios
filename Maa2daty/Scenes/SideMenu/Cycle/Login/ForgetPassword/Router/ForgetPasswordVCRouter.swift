//
//  ForgetPasswordVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/13/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class ForgetPasswordVCRouter{
    
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createForgetPasswordVC() -> UIViewController {
        
        let ForgetPasswordVC = mainStoryboard.instantiateViewController(withIdentifier: "ForgetPasswordVC")
        if let view = ForgetPasswordVC as? forgetPasswoordVCView {
            let interactor = ForgetPasswordVCInteractor()
            let router = ForgetPasswordVCRouter()
            let presenter = ForgetPasswordVCPreseneter(view: view, interactor: interactor, router: router)
            view.presenter = presenter
        }
        return ForgetPasswordVC
    }
    
    func navigateVerifyCodeVC(from view: forgetPasswoordVCView?, phone: String, isRegister: Bool) {
        let viewww = VerificationCodeVCRouter.createVerificationCodeVC(phone: phone, isRegister: isRegister)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(viewww, animated: true)
        }
    }
}
