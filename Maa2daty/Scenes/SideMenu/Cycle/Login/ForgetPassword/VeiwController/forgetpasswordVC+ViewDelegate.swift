//
//  forgetpasswordVC+ViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/13/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension ForgetPasswordVC: forgetPasswoordVCView{
    
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchData() {
        perform(#selector(pushVerificationCode), with: self, afterDelay: 1.5)
    }
    
    @objc func pushVerificationCode(){
        presenter?.navigateToVerifyCode(phone: tf_Phone.text!)
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
