//
//  ForgetPasswordVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/13/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ForgetPasswordVC: UIViewController {

    //MARK:- Variable
    var presenter: ForgetPasswordVCPreseneter?
    
    //MARK:- Outlets
    @IBOutlet weak var tf_Phone: UITextField!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- Actions
    @IBAction func sendAction(_ sender: Any) {
        presenter?.sendPressed(phone: tf_Phone)
    }
    
    //MARK:- appCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
