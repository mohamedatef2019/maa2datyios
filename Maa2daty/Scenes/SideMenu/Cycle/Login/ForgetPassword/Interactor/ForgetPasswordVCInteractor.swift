//
//  ForgetPasswordVCInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/13/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class ForgetPasswordVCInteractor{
    func ForgetPasswordRequest(param: [String: Any],completionHandler: @escaping (userResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequestWithHeader(URLs.forget, header: ["lang":SharedHandler.currentLanguage()], Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(userResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
