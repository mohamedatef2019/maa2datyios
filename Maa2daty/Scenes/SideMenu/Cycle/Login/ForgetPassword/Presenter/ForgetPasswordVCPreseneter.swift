//
//  ForgetPasswordVCPreseneter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/13/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol forgetPasswoordVCView: class {
    var presenter: ForgetPasswordVCPreseneter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchData()
    func showMsg(message: String, image: String)
}

class ForgetPasswordVCPreseneter{
    private weak var view: forgetPasswoordVCView?
    private let interactor: ForgetPasswordVCInteractor
    private let router: ForgetPasswordVCRouter
    
    init(view: forgetPasswoordVCView?, interactor: ForgetPasswordVCInteractor, router: ForgetPasswordVCRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func sendPressed(phone: UITextField){
        if SharedHandler.isValidAction(textFeilds: [phone]) && phone.text!.isPhone(){
            ForgetPasswordPreparedForRequest(phone: phone.text!)
        }else{
            self.view?.showMsg(message: "Please, Enter Valid Phone".localized, image: "warning")
        }
    }
    
    func ForgetPasswordPreparedForRequest(phone: String){
        view?.showIndicator()
        interactor.ForgetPasswordRequest(param: ["phone":phone]) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.view?.fetchData()
                    self.view?.showMsg(message: res!.message, image: "like")
                }else{
                    self.view?.showMsg(message: res!.message, image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    func navigateToVerifyCode(phone: String){
        router.navigateVerifyCodeVC(from: view, phone: phone, isRegister: false)
    }
}
