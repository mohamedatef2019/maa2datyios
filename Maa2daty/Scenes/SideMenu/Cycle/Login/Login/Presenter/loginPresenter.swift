//
//  loginPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/13/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol LoginVCView: class {
    var presenter: loginPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchingDataSuccess()
    func showMsg(message: String, image: String)
}
class loginPresenter{
    
    private weak var view: LoginVCView?
    private let interactor: loginInteractor
    private let router: LoginRouter
    
    init(view: LoginVCView?, interactor: loginInteractor, router: LoginRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func configurationFields(tf_Mob: UITextField, tf_Password: UITextField){
        if SharedHandler.isValidAction(textFeilds: [tf_Mob, tf_Password]){
            LoginRequest(phone: tf_Mob.text!, pass: tf_Password.text!)
        }else{
            self.view?.showMsg(message: "All Fields is required".localized, image: "warning")
        }
    }
    
    func LoginRequest(phone: String, pass: String){
        let param = [
            "phone":phone,
            "password":pass,
            "firebase_token":SharedHandler.getDeviceToken()
        ]
        view?.showIndicator()
        interactor.LoginRequest(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    UserDefaults.standard.set(true, forKey: "isLogged")
                    SharedHandler.settingUserDefauls(userData: res!.data)
                    self.view?.fetchingDataSuccess()
                    
                }else{
                    self.view?.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    func SocialLogin(provider: String, userName: String, mail: String, socialID: String, profile: String){
        let param = [
            "provider":provider,
            "username":userName,
            "email":mail,
            "auth_id":socialID,
            "profile_picture":profile
        ]
        self.view?.showIndicator()
        interactor.SocialLoginRequest(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    UserDefaults.standard.set(true, forKey: "isLogged")
                    SharedHandler.settingUserDefauls(userData: res!.data)
                    self.view?.fetchingDataSuccess()
                    
                }else{
                    self.view?.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    func navigateToForgetPassword(){
        router.navigateForgetPasswordVC(from: view)
    }
    
    func navigateToRegisterVC(){
        router.navigateRegisterVC(from: view)
    }
}
