//
//  LoginVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/12/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import NVActivityIndicatorView

class LoginVC: UIViewController {

    //MARK:- Variable
    var presenter: loginPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var tf_Phone: UITextField!
    @IBOutlet weak var tf_Password: UITextField!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- Actions
    @IBAction func skipAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func forgetPasswordAction(_ sender: Any) {
        presenter?.navigateToForgetPassword()
    }
    
    @IBAction func loginAction(_ sender: Any) {
        presenter?.configurationFields(tf_Mob: tf_Phone, tf_Password: tf_Password)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        presenter?.navigateToRegisterVC()
    }
    
    @IBAction func facebookAction(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    
    @IBAction func googleAction(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let interactor = loginInteractor()
        let router = LoginRouter()
        presenter = loginPresenter(view: self, interactor: interactor, router: router)
        
        MenuRouter.CreateMenu(ViewController: self)
    }

}


extension LoginVC{
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, gender, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let response = result as! NSDictionary
                    
                    let email = response["email"] as! String
                    let fbID = response["id"] as! String
                    let fName = response["first_name"] as! String
                    let lName = response["last_name"] as! String
                    
                    let picture = response["picture"] as! NSDictionary
                    let data = picture["data"] as! NSDictionary
                    let urlPicture = data["url"] as! String
                    self.presenter?.SocialLogin(provider: "facebook", userName: "\(fName) \(lName)", mail: email, socialID: fbID, profile: urlPicture)
                }
                else{
//                    self.StopAnimation(spinner: self.spinner)
//                    self.showMsg(message: "خطأ غير متوقع , برجاء إعادة المحاولة", image: #imageLiteral(resourceName: "warning"))
                }
            })
        }
    }
}
