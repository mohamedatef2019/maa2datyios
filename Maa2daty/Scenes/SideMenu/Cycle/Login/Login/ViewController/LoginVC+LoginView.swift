//
//  LoginVC=LoginView.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/13/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension LoginVC: LoginVCView{
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchingDataSuccess() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

    
}
