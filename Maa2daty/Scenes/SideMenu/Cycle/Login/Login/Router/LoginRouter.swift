//
//  LoginRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/13/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class LoginRouter{
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    func navigateForgetPasswordVC(from view: LoginVCView?) {
        let ForgetPasswordView = ForgetPasswordVCRouter.createForgetPasswordVC()
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(ForgetPasswordView, animated: true)
        }
    }
    
    func navigateRegisterVC(from view: LoginVCView?) {
        let registerView = RegisterationVCRouter.createRegisterVC()
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(registerView, animated: true)
        }
    }

}
