//
//  ChangePasswordInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class changePasswordRouter{
    class func createChangePasswordVC() -> UIViewController {
        
        let ChangePasswordVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordVC")
        if let view = ChangePasswordVC as? changePasswordView {
            let interactor = ChangePassordInteractor()
            let router = changePasswordRouter()
            let presenter = ChangePasswordPresenter(view: view, router: router, interactor: interactor)
            view.presenter = presenter
        }
        return ChangePasswordVC
    }
}
