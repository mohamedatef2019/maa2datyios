//
//  ChangePasswordVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ChangePasswordVC: UIViewController {

    //MARK:- Variable
    var presenter: ChangePasswordPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var tf_ConfirmPassword: UITextField!
    @IBOutlet weak var tf_OldPassword: UITextField!
    @IBOutlet weak var tf_NewPaswword: UITextField!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var view_OldPassword: customView!
    
    //MARK:- Actions
    @IBAction func changePasswordAction(_ sender: Any) {
        presenter?.validationOnPasswords(oldPassword: tf_OldPassword.text!, newPassword: tf_NewPaswword.text!, confirmPassword: tf_ConfirmPassword.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !SharedHandler.isLogged(){
            view_OldPassword.isHidden = true
        }
    }

}
