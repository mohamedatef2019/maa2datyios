//
//  ChangePasswordVC+ChangePasswordView.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation


extension ChangePasswordVC: changePasswordView{
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchDataSuccess() {
        self.dismiss(animated: true, completion: nil)
        UserDefaults.standard.set(true, forKey: "isLogged")
        NotificationCenter.default.post(name: NSNotification.Name("refresh"), object: self)
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
