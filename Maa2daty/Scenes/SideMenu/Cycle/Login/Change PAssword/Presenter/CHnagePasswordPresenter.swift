//
//  CHnagePasswordPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol changePasswordView: class {
    var presenter: ChangePasswordPresenter? { get set}
    func showIndicator()
    func hideIndicator()
    func fetchDataSuccess()
    func showMsg(message: String, image: String)
}
class ChangePasswordPresenter{
    
    private weak var view:changePasswordView?
    private let router: changePasswordRouter
    private let interactor: ChangePassordInteractor
    
    init(view: changePasswordView, router: changePasswordRouter, interactor: ChangePassordInteractor){
        self.view = view
        self.router = router
        self.interactor = interactor
    }
    
    func validationOnPasswords(oldPassword: String, newPassword: String, confirmPassword: String){
        if newPassword == confirmPassword{
            PrepareForChangePassword(oldPassword: oldPassword, newPassword: newPassword)
        }else{
            self.view?.showMsg(message: "Passwords Didn't match".localized, image: "warning")
        }
    }
    
    private func PrepareForChangePassword(oldPassword: String, newPassword: String){
        if SharedHandler.isLogged(){
            changePassword(oldPassword: oldPassword, newPassword: newPassword, From: "profile")
        }else{
            changePassword(oldPassword: "", newPassword: newPassword, From: "login")
        }
    }
    
    private func changePassword(oldPassword: String, newPassword: String, From: String){
        let param = [
            "user_id":"\(SharedHandler.getUserID())",
            "old_password":oldPassword,
            "new_password":newPassword,
            "jwt_token":SharedHandler.getUserJwt(),
            "from":From
        ]
        view?.showIndicator()
        interactor.changePasswordRequest(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.view?.fetchDataSuccess()
                    self.view?.showMsg(message: res!.message, image: "like")
                }else{
                    self.view?.showMsg(message: res!.message, image: "warning")
                }
            }else{

            }
            self.view?.hideIndicator()
        }
    }
}
