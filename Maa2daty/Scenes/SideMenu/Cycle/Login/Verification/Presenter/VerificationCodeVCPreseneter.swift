//
//  VerificationCodeVCPreseneter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/14/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol VerificationCodeVCView: class {
    var presenter: VerificationCodeVCPreseneter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchData()
    func dismissCycle()
    func showMsg(message: String, image: String)
}

class VerificationCodeVCPreseneter{
    private weak var view: VerificationCodeVCView?
    private let interactor: VerificationCodeVCInteractor
    private let router: VerificationCodeVCRouter
    private let phone: String
    private let isRegister: Bool
    
    init(view: VerificationCodeVCView?, interactor: VerificationCodeVCInteractor, router: VerificationCodeVCRouter, phone: String, isRegister: Bool) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.phone = phone
        self.isRegister = isRegister
    }
    
    func VerifyCode(code: String){
        view?.showIndicator()
        interactor.verificationCodeRequest(param: ["activate_code":code, "phone":phone]) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.view?.fetchData()
                    SharedHandler.settingUserDefauls(userData: res!.data)
                    self.view?.showMsg(message: res!.message, image: "like")
                }else{
                    self.view?.showMsg(message: res!.message, image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    func navigateToNextVC(){
        if isRegister{
            self.view?.dismissCycle()
        }else{
            router.navigateChangePasswordVC(from: view, phone: phone)
        }
    }
}
