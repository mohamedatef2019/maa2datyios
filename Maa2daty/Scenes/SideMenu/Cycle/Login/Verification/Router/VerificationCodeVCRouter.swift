//
//  VerificationCodeVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/14/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class VerificationCodeVCRouter{
    class func createVerificationCodeVC(phone: String, isRegister: Bool) -> UIViewController {
        
        let VerificationCodeVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerificationCodeVC")
        if let view = VerificationCodeVC as? VerificationCodeVCView {
            let interactor = VerificationCodeVCInteractor()
            let router = VerificationCodeVCRouter()
            let presenter = VerificationCodeVCPreseneter(view: view, interactor: interactor, router: router, phone: phone, isRegister: isRegister)
            view.presenter = presenter
        }
        return VerificationCodeVC
    }
    
    func navigateChangePasswordVC(from view: VerificationCodeVCView?, phone: String) {
        let viewww = changePasswordRouter.createChangePasswordVC()
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(viewww, animated: true)
        }
    }
}
