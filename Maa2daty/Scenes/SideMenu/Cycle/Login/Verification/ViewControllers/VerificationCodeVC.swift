
//
//  VerificationCodeVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/14/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import PinCodeTextField
import NVActivityIndicatorView

class VerificationCodeVC: UIViewController {

    //MARK:- Variable
    var presenter: VerificationCodeVCPreseneter?
    @IBOutlet weak var tf_code: PinCodeTextField!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- Outlets
    
    //MARK:- Actions
    @IBAction func sendCodeAction(_ sender: Any) {
        presenter?.VerifyCode(code: tf_code.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

}
