//
//  verificationVC+VerificationViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension VerificationCodeVC: VerificationCodeVCView{
    func dismiss() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchData() {
        perform(#selector(pushNextVC), with: self, afterDelay: 1.5)
    }
    
    @objc func pushNextVC(){
        presenter?.navigateToNextVC()
    }
    
    func dismissCycle() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
