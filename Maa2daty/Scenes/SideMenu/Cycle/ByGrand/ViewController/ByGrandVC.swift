//
//  ByGrandVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/12/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import KMPopUp

class ByGrandViewController: UIViewController {
    
    @IBOutlet weak var whatsAppLbl: UILabel!
    @IBOutlet weak var designLbl: UILabel!
    @IBOutlet weak var callLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        showAnimate()
        whatsAppLbl.text = "whatsapp".localized
        callLbl.text = "call".localized
        designLbl.text = "design and Develop".localized
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func closeButton(_ sender: Any) {
        removeAnimation()
    }
    
    @IBAction func openSite(_ sender: Any) {
        if let url = URL(string: "http://2grand.net") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func call(_ sender: Any) {
        let url:NSURL = NSURL(string: "tel://00201157771069")!
        UIApplication.shared.openURL(url as URL)
    }
    
    @IBAction func whatsApp(_ sender: Any) {
        let date = Date()
        let msg = "Hello \(date)"
        let urlWhats = "whatsapp://send?phone=+201157771069&abid=00201157771069&text=\(msg)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                } else {
                    SharedHandler.showMsg(message: "warning".localized, image: "warning", ViewController: self)
                }
            }
        }
    }
}

