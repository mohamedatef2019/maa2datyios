//
//  OffersVC+OffersViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension OffersVC: OffersVCView{
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchDataSuccess() {
        coll_Offers.reloadData()
    }
    
    func shareProduct(Link: String) {
        shareLink(url: Link)
    }
    
    func reloadCollection() {
        coll_Offers.reloadData()
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
