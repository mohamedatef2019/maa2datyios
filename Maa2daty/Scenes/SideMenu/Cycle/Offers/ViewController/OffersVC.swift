//
//  OffersVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OffersVC: UIViewController {

    
    //MARK:- Variable
    var presenter: OffersVCPresenter?
    
    
    //MARK:- Outlets
    @IBOutlet weak var coll_Offers: UICollectionView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- Actions
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBTNs()
        setupCollectionView()
        let interactor = OffersVCInteractor()
        let router = OffersVCRouter()
        presenter = OffersVCPresenter(view: self, interactor: interactor, router: router)
        presenter?.didLoad()
        MenuRouter.CreateMenu(ViewController: self)
    }
}
