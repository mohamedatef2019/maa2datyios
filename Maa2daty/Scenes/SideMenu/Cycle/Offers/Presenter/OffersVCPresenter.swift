//
//  OffersVCPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol OffersVCView : class{
    var presenter: OffersVCPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchDataSuccess()
    func shareProduct(Link: String)
    func reloadCollection()
    func showMsg(message: String, image: String)
}
class OffersVCPresenter{
    private var arrProducts: [SingleProduct]?
    private weak var view: OffersVCView?
    private let interactor: OffersVCInteractor
    private let router: OffersVCRouter
    
    init(view: OffersVCView, interactor: OffersVCInteractor, router: OffersVCRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func didLoad(){
        view?.showIndicator()
        interactor.LoadData { (res, error) in
            if error == nil{
                self.arrProducts = res?.products.singleProduct
                self.view?.fetchDataSuccess()
                if self.arrProducts?.count == 0{
                    SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                }
            }else{
                SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                self.view?.showMsg(message: "Please, Try again later".localized, image: "warning")
                
            }
            self.view?.hideIndicator()
        }
    }
    func getProductsCount() -> Int{
        return arrProducts?.count ?? 0
    }
    
    func configure(cell: ProductCell, for index: Int) {
        let SingleProduct = arrProducts?[index]
        cell.SettingCosmosValue(Rate: Double(SingleProduct?.rate ?? "0") ?? 0)
        cell.SettingProductName(Name: SingleProduct?.name ?? "")
        cell.SettingProductImage(Image: SingleProduct?.product_image ?? "")
        cell.SettingProductPrice(Price: String(SingleProduct?.price ?? 0))
        cell.SettingProductFavourite(isFav: SingleProduct?.user_favorite ?? 0)
        cell.SettingProductDescription(Desc: SingleProduct?.description ?? "")
        
        cell.btn_Cart.tag = index
        cell.btn_Share.tag = index
        cell.btn_isFavouriteProduct.tag = index
        
        cell.btn_Cart.addTarget(self, action: #selector(PressedCart(sender:)), for: UIControl.Event.touchUpInside)
        cell.btn_Share.addTarget(self, action: #selector(shareProduct(sender:)), for: UIControl.Event.touchUpInside)
        cell.btn_isFavouriteProduct.addTarget(self, action: #selector(setFavourite(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    func LoadSingleProductDetails(index: Int){
        view?.showIndicator()
        interactor.LoadProductDetails(ProductID: arrProducts?[index].id ?? 0) { (res, error) in
            if error == nil{
                self.router.navigateToProductsDetailsScreen(from: self.view, response: res!, title: self.arrProducts?[index].name ?? "")
            }else{
                self.view?.showMsg(message: "Sorry, There is no item".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    @objc func PressedCart(sender: UIButton){
        addToCart(index: sender.tag)
    }
    @objc func setFavourite(sender: UIButton){
        favouriteProduct(index: sender.tag)
    }
    @objc func shareProduct(sender: UIButton){
        shareProduct(index: sender.tag)
    }
    
    func addToCart(index: Int){
        view?.showIndicator()
        if SharedHandler.isLogged(){
            let param = [
                "user_id":SharedHandler.getUserID(),
                "product_id":"\(arrProducts?[index].id ?? 0)",
                "qty":"1",
                "jwt_token":"\(SharedHandler.getUserJwt())"
                ] as [String : Any]
            interactor.addProductToCart(param: param) { (res, error) in
                if error == nil{
                    if res?.status == 200{
                        self.view?.showMsg(message: res?.message ?? "", image: "like")
                    }else{
                        self.view?.showMsg(message: res?.message ?? "", image: "warning")
                    }
                }else{
                    self.view?.showMsg(message: "Please, Try Again Later".localized, image: "warning")
                }
                self.view?.hideIndicator()
            }
        }else{
            self.view?.showMsg(message: "Please, Login first".localized, image: "warning")
        }
    }
    
    func favouriteProduct(index: Int){
        view?.showIndicator()
        if SharedHandler.isLogged(){
            let param = [
                "user_id":SharedHandler.getUserID(),
                "product_id":"\(arrProducts?[index].id ?? 0)",
                "jwt_token":SharedHandler.getUserJwt()
                ] as [String : Any]
            interactor.favouriteProduct(param: param) { (res, error) in
                if error == nil{
                    if res?.status == 200{
                        if self.arrProducts![index].user_favorite == 0{
                            self.arrProducts![index].user_favorite = 1
                        }else{
                            self.arrProducts![index].user_favorite = 0
                        }
                        self.view?.reloadCollection()
                        self.view?.showMsg(message: res?.message ?? "", image: "like")
                    }else{
                        self.view?.showMsg(message: res?.message ?? "", image: "warning")
                    }
                }else{
                    self.view?.showMsg(message: "Please, Try Again Later".localized, image: "warning")
                }
                self.view?.hideIndicator()
            }
        }else{
            self.view?.showMsg(message: "Please, Login first".localized, image: "warning")
        }
    }
    
    func shareProduct(index: Int){
        view?.shareProduct(Link: arrProducts?[index].share_link ?? "")
    }
}
