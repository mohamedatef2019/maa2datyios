//
//  OffersVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class OffersVCRouter{
    func navigateToProductsDetailsScreen(from view: OffersVCView?, response: SingleProductDetailsResponse, title: String) {
        let singleProduct = SingleProductDetailsRouter.createProductsDetailsVC(response: response, Title: title)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(singleProduct, animated: true)
        }
    }
}
