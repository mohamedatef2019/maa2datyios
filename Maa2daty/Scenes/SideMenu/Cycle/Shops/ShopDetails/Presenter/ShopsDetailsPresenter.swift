//
//  ShopsDetailsPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol ShopDetailsView: class {
    var presenter: ShopsDetailsPresenter? { get set }
//    func showIndicator()
//    func dismissIndicator()
    func SettingProductDescription(desc: String)
    func settingShopImage(image: String)
    func settingShopBranches(branches: [Branches])
}

class ShopsDetailsPresenter{
    private var response: AllShops?
    private weak var view: ShopDetailsView?
    private let router : ShopsDetailsRouter
    
    init(view: ShopDetailsView, response: AllShops, router: ShopsDetailsRouter){
        self.response = response
        self.view = view
        self.router = router
    }
    
    func didLoad(){
        view?.settingShopImage(image: response?.image ?? "")
        view?.SettingProductDescription(desc: response?.description ?? "")
        view?.settingShopBranches(branches: response?.branches ?? [])
    }
    
    func getshopCategory(){
        router.navigateCategoryScreen(from: view, url: URLs.categoryShop + "\(response?.id ?? 0)", shopID: response?.id ?? -1)
    }
}
