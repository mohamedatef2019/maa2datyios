//
//  shopsDetailsVC+shopsDetailsViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation
import MapKit

extension ShopDetailsVC: ShopDetailsView{
    
    
    func SettingProductDescription(desc: String) {
        lbl_Description.text = desc
    }
    
    func settingShopImage(image: String) {
        setImage(url: image, picture: imge_Shop)
    }
    
    func settingShopBranches(branches: [Branches]) {
        for i in 0..<branches.count{
            let branch = CityLocation(title: branches[i].address, coordinate: CLLocationCoordinate2D(latitude: Double(branches[i].lat) ?? 0.0, longitude: Double(branches[i].lng) ?? 0.0))
            
            map_Branches.addAnnotation(branch)
        }
    }
    
}
