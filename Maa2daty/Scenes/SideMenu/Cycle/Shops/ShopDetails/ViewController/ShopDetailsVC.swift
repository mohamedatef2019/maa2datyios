//
//  ShopDetailsVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import MapKit
import NVActivityIndicatorView

class ShopDetailsVC: UIViewController {

    //MARK:- Variable
    var presenter: ShopsDetailsPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var imge_Shop: RoundImage!
    @IBOutlet weak var map_Branches: MKMapView!
    @IBOutlet weak var lbl_Description: UILabel!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- Actions
    @IBAction func OpenProductsAction(_ sender: Any) {
        presenter?.getshopCategory()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.didLoad()
    }

}
