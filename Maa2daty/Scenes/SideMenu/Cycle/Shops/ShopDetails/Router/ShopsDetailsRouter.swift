//
//  ShopsDetailsRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class ShopsDetailsRouter{
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createProductsDetailsVC(response: AllShops) -> UIViewController {
        
        let shopsDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "ShopDetailsVC")
        if let view = shopsDetailsVC as? ShopDetailsView {
            let router = ShopsDetailsRouter()
            let presenter = ShopsDetailsPresenter(view: view, response: response, router: router)
            view.presenter = presenter
        }
        return shopsDetailsVC
    }
    
    func navigateCategoryScreen(from view: ShopDetailsView?, url: String, shopID: Int) {
        let categoryView = CategoryVCRouter.createCategoryVC(url: url, shopID: shopID)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(categoryView, animated: true)
        }
    }
}
