//
//  ShopsVCInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class ShopsVCInteractor{
    func LoadData(completionHandler: @escaping (shopsResponse?, Error?) -> ()){
        APIManager.sharedInstance.getRequestWithLanguage(URLs.allShops, headers: ["lang":SharedHandler.currentLanguage()]) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(shopsResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
