//
//  ShopsVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class ShopsVCRouter{
    
    func navigateToShopDetailsScreen(from view: ShopsVCView?, singleShop: AllShops) {
        let subCategoryView = ShopsDetailsRouter.createProductsDetailsVC(response: singleShop)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(subCategoryView, animated: true)
        }
    }
}
