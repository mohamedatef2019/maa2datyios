//
//  ShopsVCPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol ShopsVCView: class {
    
    var presenter: ShopsVCPresenter? { get set }
    func showIndicator()
    func dismissIndicator()
    func fetchDataSuccess()
    func showMsg(message: String, image: String)
}

protocol ShopsVCCell {
    func settingShopName(Name: String)
    func settingShopImage(image: String)
}

class ShopsVCPresenter{
    private var selectedIndex = -1
    private var Response: shopsResponse?
    private weak var view: ShopsVCView?
    private let router: ShopsVCRouter
    private let interactor: ShopsVCInteractor
    
    init(view: ShopsVCView, router: ShopsVCRouter, interactor: ShopsVCInteractor) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
    
    func didLoad(){
        LoadData()
    }
    
    func LoadData(){
        view?.showIndicator()
        interactor.LoadData { (Response, Error) in
            if Error == nil{
                if Response?.status == 200{
                    self.Response = Response
                    if self.Response?.data.count == 0{
                        SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                    }else{
                        self.view?.fetchDataSuccess()
                    }
                }else{
                    SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                    self.view?.showMsg(message: Response?.message ?? "Please, Try again later".localized, image: "warning")
                }
            }else{
                SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                self.view?.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view?.dismissIndicator()
        }
    }
    
    func getShopsCount() -> Int{
        return Response?.data.count ?? 0
    }
    
    func configurationShopCell(index: Int, cell: ShopsCell){
        cell.settingShopImage(image: Response?.data[index].image ?? "")
        cell.settingShopName(Name: Response?.data[index].name ?? "")
    }
    
    func didSelectSingleShop(index: Int){
        selectedIndex = index
        router.navigateToShopDetailsScreen(from: view, singleShop: Response!.data[index])
    }
    
    func deselectTableRow() -> Int{
        let returnValue = selectedIndex
        self.selectedIndex = -1
        return returnValue
    }
}
