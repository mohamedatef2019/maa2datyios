//
//  shopsVC+tableViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension ShopsVC: UITableViewDelegate, UITableViewDataSource{
    func setUpTableView(){
        tbl_Shops.delegate = self
        tbl_Shops.dataSource = self
        
        tbl_Shops.register(UINib(nibName: "ShopsCell", bundle: nil), forCellReuseIdentifier: "ShopsCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getShopsCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_Shops.dequeueReusableCell(withIdentifier: "ShopsCell") as! ShopsCell
        presenter?.configurationShopCell(index: indexPath.row, cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.didSelectSingleShop(index: indexPath.row)
    }
    
}
