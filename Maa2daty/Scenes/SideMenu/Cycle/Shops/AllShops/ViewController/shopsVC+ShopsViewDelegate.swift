//
//  shopsVC+ShopsViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension ShopsVC: ShopsVCView{
    
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func dismissIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchDataSuccess() {
        tbl_Shops.reloadData()
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
