//
//  ShopsVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ShopsVC: UIViewController {

    //MARK:- Variable
    var presenter: ShopsVCPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var tbl_Shops: UITableView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBTNs()
        
        let interactor = ShopsVCInteractor()
        let router = ShopsVCRouter()
        presenter = ShopsVCPresenter(view: self, router: router, interactor: interactor)
        
        
        setUpTableView()
        presenter?.didLoad()
        MenuRouter.CreateMenu(ViewController: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let row = presenter?.deselectTableRow() , row != -1{
            tbl_Shops.deselectRow(at: IndexPath(row: row, section: 0), animated: true)
        }
    }
}
