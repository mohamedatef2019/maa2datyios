//
//  ShopsCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class ShopsCell: UITableViewCell {

    @IBOutlet weak var lbl_ShopName: UILabel!
    @IBOutlet weak var img_ShopImage: RoundImage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

extension ShopsCell: ShopsVCCell{
    func settingShopName(Name: String) {
        lbl_ShopName.text = Name
    }
    
    func settingShopImage(image: String) {
        setImage(url: image, picture: img_ShopImage)
    }
    
    
}
