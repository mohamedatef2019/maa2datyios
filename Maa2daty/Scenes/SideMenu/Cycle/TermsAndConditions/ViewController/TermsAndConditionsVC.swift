//
//  TermsAndConditionsVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/12/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import WebKit

class TermsAndConditionsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBTNs()
        
        let request = URLRequest(url: URL(string: URLs.termsAndConditions)!)
        webView.load(request)

        MenuRouter.CreateMenu(ViewController: self)
    }
}
