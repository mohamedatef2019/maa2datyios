
//
//  ProductCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {

    @IBOutlet weak var view_Cosmos: CosmosView!
    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet weak var lbl_ProductName: UILabel!
    @IBOutlet weak var lbl_ProductPrice: UILabel!
    @IBOutlet weak var lbl_ProductDescription: UILabel!
    @IBOutlet weak var btn_isFavouriteProduct: UIButton!
    @IBOutlet weak var btn_Share: UIButton!
    @IBOutlet weak var btn_Cart: UIButton!
    
    @IBAction func FavouriteAction(_ sender: Any) {
    }
    @IBAction func shareAction(_ sender: Any) {
    }
    @IBAction func addToCart(_ sender: Any) {
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
extension ProductCell: ProductCellView{
    func SettingCosmosValue(Rate: Double) {
        view_Cosmos.rating = Rate
    }
    
    func SettingProductName(Name: String) {
        lbl_ProductName.text = Name
    }
    
    func SettingProductPrice(Price: String) {
        lbl_ProductPrice.text = Price
    }
    
    func SettingProductImage(Image: String) {
        setImage(url: Image, picture: img_Product)
    }
    
    func SettingProductFavourite(isFav: Int) {
        if isFav == 1{
            btn_isFavouriteProduct.setImage(UIImage(named: "like (-1"), for: UIControl.State.normal)
        }else{
            btn_isFavouriteProduct.setImage(UIImage(named: "heart"), for: UIControl.State.normal)
        }
    }
    
    func SettingProductDescription(Desc: String) {
        lbl_ProductDescription.text  = Desc
    }
    
}
