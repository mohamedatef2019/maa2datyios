//
//  ProductsVCPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol ProductView: class {
    var presenter: ProductsVCPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchingDataSuccess()
    func shareProduct(Link: String)
    func reloadCollection()
    func showMsg(message: String, image: String)
}

protocol ProductCellView {
    func SettingCosmosValue(Rate: Double)
    func SettingProductName(Name: String)
    func SettingProductPrice(Price: String)
    func SettingProductImage(Image: String)
    func SettingProductFavourite(isFav: Int)
    func SettingProductDescription(Desc: String)
}

class ProductsVCPresenter{
    
    private var selectIndex = 0
    private var Title: String?
    private var arrProducts: [SingleProduct]?
    private weak var view: ProductView?
    private let interactor: productsInteractor
    private let router: ProductsVCRouter
    
    init(arProducts: [SingleProduct], view: ProductView, interactor: productsInteractor, router: ProductsVCRouter, Title: String){
        self.arrProducts = arProducts
        self.view = view
        self.interactor = interactor
        self.router = router
        self.Title = Title
    }
    
    func getTitle() -> String{
        return Title ?? ""
    }
    
    func getProductsCount() -> Int{
        return arrProducts?.count ?? 0
    }
    
    func configure(cell: ProductCell, for index: Int) {
        let SingleProduct = arrProducts?[index]
        cell.SettingCosmosValue(Rate: Double(SingleProduct?.rate ?? "0") ?? 0)
        cell.SettingProductName(Name: SingleProduct?.name ?? "")
        cell.SettingProductImage(Image: SingleProduct?.product_image ?? "")
        cell.SettingProductPrice(Price: String(SingleProduct?.price ?? 0))
        cell.SettingProductFavourite(isFav: SingleProduct?.user_favorite ?? 0)
        cell.SettingProductDescription(Desc: SingleProduct?.description ?? "")
        
        cell.btn_Cart.tag = index
        cell.btn_Share.tag = index
        cell.btn_isFavouriteProduct.tag = index
        
        cell.btn_Cart.addTarget(self, action: #selector(PressedCart(sender:)), for: UIControl.Event.touchUpInside)
        cell.btn_Share.addTarget(self, action: #selector(shareProduct(sender:)), for: UIControl.Event.touchUpInside)
        cell.btn_isFavouriteProduct.addTarget(self, action: #selector(setFavourite(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    func LoadSingleProductDetails(index: Int){
        view?.showIndicator()
        interactor.LoadProductDetails(ProductID: arrProducts?[index].id ?? 0) { (res, error) in
            if error == nil{
                self.selectIndex = index
                self.router.navigateToProductsDetailsScreen(from: self.view, response: res!, title: self.arrProducts?[index].name ?? "")
            }else{
                self.view?.showMsg(message: "Sorry, There is no item".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    func reloadSpecificRow(){
        
    }
    
    @objc func PressedCart(sender: UIButton){
        addToCart(index: sender.tag)
    }
    
    @objc func setFavourite(sender: UIButton){
        favouriteProduct(index: sender.tag)
    }
    
    @objc func shareProduct(sender: UIButton){
        shareProduct(index: sender.tag)
    }
    
    func addToCart(index: Int){
        view?.showIndicator()
        if SharedHandler.isLogged(){
            let param = [
                "user_id":SharedHandler.getUserID(),
                "product_id":arrProducts?[index].id,
                "qty":"1",
                "jwt_token":"\(SharedHandler.getUserJwt())"
                ] as [String : Any]
            
            interactor.addProductToCart(param: param) { (res, error) in
                if error == nil{
                    if res?.status == 200{
                        self.view?.showMsg(message: res?.message ?? "", image: "like")
                    }else{
                        self.view?.showMsg(message: res?.message ?? "", image: "warning")
                    }
                }else{
                    self.view?.showMsg(message: "Please, Try Again Later".localized, image: "warning")
                }
                self.view?.hideIndicator()
            }
        }else{
            self.view?.showMsg(message: "Please, Login first".localized, image: "warning")
        }
    }
    
    func favouriteProduct(index: Int){
        view?.showIndicator()
        if SharedHandler.isLogged(){
            let param = [
                "user_id":SharedHandler.getUserID(),
                "product_id":arrProducts?[index].id,
                "jwt_token":SharedHandler.getUserJwt()
                ] as [String : Any]
            interactor.favouriteProduct(param: param) { (res, error) in
                if error == nil{
                    if res?.status == 200{
                        if self.arrProducts![index].user_favorite == 0{
                        self.arrProducts![index].user_favorite = 1
                    }else{
                        self.arrProducts![index].user_favorite = 0
                        }
                        self.view?.reloadCollection()

                        self.view?.showMsg(message: res?.message ?? "", image: "like")
                    }else{
                        self.view?.showMsg(message: res?.message ?? "", image: "warning")
                    }
                }else{
                    self.view?.showMsg(message: "Please, Try Again Later".localized, image: "warning")
                }
                self.view?.hideIndicator()
            }
        }else{
            self.view?.showMsg(message: "Please, Login first".localized, image: "warning")
        }
    }
    
    func shareProduct(index: Int){
        view?.shareProduct(Link: arrProducts?[index].share_link ?? "")
    }
}
