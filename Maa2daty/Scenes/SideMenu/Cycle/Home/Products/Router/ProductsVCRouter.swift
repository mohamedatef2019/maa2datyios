//
//  ProductsVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class ProductsVCRouter{
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createProductsVC(Products: [SingleProduct], Title: String) -> UIViewController {
        
        let ProductsVC = mainStoryboard.instantiateViewController(withIdentifier: "ProductsVC")
        if let ProductView = ProductsVC as? ProductView {
            let interactor = productsInteractor()
            let router = ProductsVCRouter()
            let presenter = ProductsVCPresenter(arProducts: Products, view: ProductView, interactor: interactor, router: router, Title: Title)
            ProductView.presenter = presenter
        }
        return ProductsVC
    }
    func navigateToProductsDetailsScreen(from view: ProductView?, response: SingleProductDetailsResponse, title: String) {
        let singleProduct = SingleProductDetailsRouter.createProductsDetailsVC(response: response, Title: title)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(singleProduct, animated: true)
        }
    }
}
