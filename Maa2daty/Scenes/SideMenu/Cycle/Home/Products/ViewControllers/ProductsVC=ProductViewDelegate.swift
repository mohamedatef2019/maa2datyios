//
//  ProductsVC=ProductViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension ProductsVC: ProductView{
    
    func shareProduct(Link: String) {
        shareLink(url: Link)
    }
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchingDataSuccess() {
        coll_Products.reloadData()
    }
    
    func reloadCollection() {
        coll_Products.reloadData()
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }
}
