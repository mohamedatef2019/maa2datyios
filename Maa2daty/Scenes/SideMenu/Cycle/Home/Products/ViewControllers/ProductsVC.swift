//
//  ProductsVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ProductsVC: UIViewController {

    //MARK:- Variable
    var presenter: ProductsVCPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var coll_Products: UICollectionView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        title = presenter?.getTitle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if presenter?.getProductsCount() == 0 {
            emptyLabel(self.view, msg: "No Data".localized)
        }
    }
}
