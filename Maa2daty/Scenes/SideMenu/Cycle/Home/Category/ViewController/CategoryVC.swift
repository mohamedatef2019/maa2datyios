//
//  CategoryVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CategoryVC: UIViewController {
    
    //MARK:- Variable
    var presenter: CategoryVCPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var coll_Categories: UICollectionView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- AppCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if presenter == nil{
            title = "Home".localized
            let interactor = CategoryVCInteractor()
            let router = CategoryVCRouter()
            presenter = CategoryVCPresenter(view: self, interactor: interactor, router: router, url: URLs.Category, shopID: -1)
            
            MenuRouter.CreateMenu(ViewController: self)
        }
        setupCollectionView()
        presenter?.didLoad()
        presenter?.isChild()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationBTNs()
    }

}
