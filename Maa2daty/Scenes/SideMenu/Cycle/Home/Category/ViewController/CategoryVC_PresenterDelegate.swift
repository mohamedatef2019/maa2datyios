//
//  CategoryVC_PresenterDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension CategoryVC: CategoryView{
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchingDataSuccess() {
        coll_Categories.reloadData()
    }

    func isChild() {
        if SharedHandler.isGrand(){
            UserDefaults.standard.set(false, forKey: "isGrand")
            addChild(id: "ByGrandViewController")
        }else if SharedHandler.isChange(){
            UserDefaults.standard.set(false, forKey: "isChange")
            addChild(id: "changeLanguageVC")
        }
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
