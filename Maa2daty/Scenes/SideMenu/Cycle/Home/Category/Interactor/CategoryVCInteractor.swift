//
//  HomeVCInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class CategoryVCInteractor{
    func LoadData(url: String,completionHandler: @escaping (CategoryResponse?, Error?) -> ()){
        APIManager.sharedInstance.getRequestWithLanguage(url, headers: ["lang":SharedHandler.currentLanguage()]) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(CategoryResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
