//
//  categoryCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class categoryCell: UICollectionViewCell, categoryCellView {
    

    @IBOutlet weak var img_Category: UIImageView!
    @IBOutlet weak var lbl_CategoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func SettingTitle(title: String) {
        lbl_CategoryName.text = title
    }
    
    func SettingImage(image: String) {
        setImage(url: image, picture: img_Category)
    }
}
