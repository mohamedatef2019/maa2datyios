//
//  HomeVCPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol CategoryView: class {
    
    var presenter: CategoryVCPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func isChild()
    func fetchingDataSuccess()
    func showMsg(message: String, image: String)
}

protocol categoryCellView {
    func SettingTitle(title: String)
    func SettingImage(image: String)
}

class CategoryVCPresenter{
    private var url: String
    private var shopID: Int
    private var Response: CategoryResponse?
    private weak var view: CategoryView?
    private let interactor: CategoryVCInteractor
    private let router: CategoryVCRouter
    
    init(view: CategoryView?, interactor: CategoryVCInteractor, router: CategoryVCRouter, url: String, shopID: Int) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.url = url
        self.shopID = shopID
    }
    
    func didLoad(){
        LoadData()
    }
    
    func LoadData(){
        view?.showIndicator()
        interactor.LoadData(url: url) { (Response, Error) in
            if Error == nil{
                self.Response = Response
                self.reloadCCollection()
                if Response?.category.count == 0{
                    SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                }
            }else{
                SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                self.view?.showMsg(message: "Sorry, There is no item".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    func reloadCCollection(){
        view?.fetchingDataSuccess()
    }
    
    func configure(cell: categoryCellView, for index: Int) {
        let singleCategory = Response?.category[index]
        cell.SettingImage(image: singleCategory?.category_image ?? "")
        cell.SettingTitle(title: singleCategory?.name ?? "")
    }
    
    func getCategoryCount() -> Int {
        return Response?.category.count ?? 0
    }
    
    func DidSelect(index: Int){
//        if Response?.category[index].subcategory.count ?? 0 > 0{
//            router.navigateToHomeDetailsScreen(from: view, Category: Response?.category[index] ?? Category(), shopID: shopID)
//        }else{
//            self.view?.showMsg(message: "Sorry, There is no item".localized, image: "warning")
//        }
        router.navigateToHomeDetailsScreen(from: view, Category: Response?.category[index] ?? Category(), shopID: shopID)
    }
    
    func isChild(){
        view?.isChild()
    }
}
