//
//  File.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class CategoryVCRouter{
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    func navigateToHomeDetailsScreen(from view: CategoryView?, Category: Category, shopID: Int) {
        let subCategoryView = SubCategoryRouter.createSubCategoryVC(Category: Category, shopID: shopID)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(subCategoryView, animated: true)
        }
    }
    
    class func createCategoryVC(url: String, shopID: Int) -> UIViewController {
        
        let CategoryVC = mainStoryboard.instantiateViewController(withIdentifier: "CategoryVC")
        if let view = CategoryVC as? CategoryView {
            let router = CategoryVCRouter()
            let interactor = CategoryVCInteractor()
            let presenter = CategoryVCPresenter(view: view, interactor: interactor, router: router, url: url, shopID: shopID)
            view.presenter = presenter
        }
        return CategoryVC
    }
}
