//
//  SubCategoryPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol SubCategoryView: class {
    
    var presenter: SubCategoryPresenter? { get set }
    func showIndicator()
    func hideIndicator()
//    func fetchingDataSuccess()
}

class SubCategoryPresenter{
    private var shopID: Int
    private var singleSubCategory: Category?
    private weak var view: SubCategoryView?
    private let router: SubCategoryRouter
    private let interactor: SubCategoryInteractor
    
    init(SingleSubCat: Category,view: SubCategoryView?, router: SubCategoryRouter, interactor: SubCategoryInteractor, shopID: Int) {
        self.view = view
        self.router = router
        self.singleSubCategory = SingleSubCat
        self.interactor = interactor
        self.shopID = shopID
    }
    
    func getSubCategoryCount() -> Int{
        return singleSubCategory?.subcategory.count ?? 0
    }
    
    func configure(cell: categoryCellView, for index: Int) {
        let singleCategory = singleSubCategory?.subcategory[index]
        cell.SettingImage(image: singleCategory?.subcategory_image ?? "")
        cell.SettingTitle(title: singleCategory?.name ?? "")
    }
    
    func PrepareForLoadProducts(index: Int){
        if shopID == -1 {
            LoadSubCategory(url: "\(URLs.allProducts)\(singleSubCategory?.id ?? 0)/\(singleSubCategory?.subcategory[index].id ?? 0)/\(SharedHandler.getUserID())", index: index)
        }else{
            LoadSubCategory(url: "\(URLs.productsShop)\(shopID)/\(singleSubCategory?.id ?? 0)/\(singleSubCategory?.subcategory[index].id ?? 0)/\(SharedHandler.getUserID())", index: index)
        }
    }
    
    func LoadSubCategory(url: String, index: Int){
        view?.showIndicator()
        interactor.LoadData(url: url) { (res, error) in
            if error == nil{
//                if res?.products.singleProduct.count ?? 0 > 0{
//                    
//                }
                self.router.navigateToProductsScreen(from: self.view, Products: res?.products.singleProduct ?? [], title: self.singleSubCategory?.subcategory[index].name ?? "")
            }
            self.view?.hideIndicator()
        }
    }
    
    
    
    func getTitle() -> String{
        return singleSubCategory?.name ?? ""
    }
}
