//
//  subCategory+SubCategoryViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension SubCategoryVC: SubCategoryView{
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
//    func fetchingDataSuccess() {
//        coll_SubCategory.reloadData()
//        if presenter?.getSubCategoryCount() == 0 {
//            emptyLabel(self.view, msg: "No Data")
//        }
//    }
}
