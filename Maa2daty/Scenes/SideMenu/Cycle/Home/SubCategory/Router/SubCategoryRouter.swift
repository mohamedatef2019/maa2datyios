//
//  SubCategoryRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class SubCategoryRouter{
    
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createSubCategoryVC(Category: Category, shopID: Int) -> UIViewController {
        
        let SubCategoryVC = mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC")
        if let subCatView = SubCategoryVC as? SubCategoryView {
            let interactor = SubCategoryInteractor()
            let presenter = SubCategoryPresenter(SingleSubCat: Category, view: subCatView, router: SubCategoryRouter(), interactor: interactor, shopID: shopID)
            subCatView.presenter = presenter
        }
        return SubCategoryVC
    }
    
    func navigateToProductsScreen(from view: SubCategoryView?, Products: [SingleProduct], title: String) {
        let subCategoryView = ProductsVCRouter.createProductsVC(Products: Products, Title: title)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(subCategoryView, animated: true)
        }
    }
}
