//
//  ImagesCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class ImagesCell: UICollectionViewCell{

    @IBOutlet weak var photo: UIImageView!

    func imageURL(url: String) {
        setImage(url: url, picture: photo)
    }
}
