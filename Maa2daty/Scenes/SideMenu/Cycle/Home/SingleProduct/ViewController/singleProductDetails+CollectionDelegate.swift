//
//  singleProductDetails+CollectionDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension SingleProductVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextViewDelegate{
    func setupCollection(){
        coll_OtherProducts.delegate = self
        coll_OtherProducts.dataSource = self
        
        coll_scrollingImages.delegate = self
        coll_scrollingImages.dataSource = self
        
        
        coll_OtherProducts.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
        coll_scrollingImages.register(UINib(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier: "ImagesCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == coll_OtherProducts{
            return presenter?.getOtherProductCount() ?? 0
        }else{
            return presenter?.getImagesForScrolling() ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == coll_OtherProducts{
            let cell = coll_OtherProducts.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
            presenter?.configureProducts(cell: cell, for: indexPath.row)
            return cell
        }else{
            let cell = coll_scrollingImages.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as! ImagesCell
            presenter?.configureImages(index: indexPath.row, cell: cell)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == coll_OtherProducts{
            if self.view.frame.width > 450{
                return CGSize(width: (self.view.frame.width / 3), height: 295.0)
            }else{
                return CGSize(width: (self.view.frame.width / 2), height: 295.0)
            }
        }else{
            return CGSize(width: (self.view.frame.width), height: 175.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == coll_OtherProducts{
            presenter?.LoadSingleProductDetails(index: indexPath.row)
        }
    }

//    firstex
//    func firstExpandTextField(textView: UITextView){
//
//    }
}
