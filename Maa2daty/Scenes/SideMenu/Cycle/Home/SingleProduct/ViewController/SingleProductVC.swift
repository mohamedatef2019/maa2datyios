//
//  SingleProductVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SingleProductVC: UIViewController {

    //MARK:- Variable
    var imagesCount = 0
    var isfav = 0
    var presenter: SingleProductDetailsPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var coll_scrollingImages: UICollectionView!
    @IBOutlet weak var view_MyRate: CosmosView!
    @IBOutlet weak var view_currentRate: CosmosView!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Description: UILabel!
    @IBOutlet weak var coll_OtherProducts: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btn_Favourite: UIButton!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    
    //MARK:- Actions
    @IBAction func submitAction(_ sender: Any) {
        presenter?.SubmitProductRate(rate: view_MyRate.rating)
    }
    
    @IBAction func addToCartAction(_ sender: Any) {
        presenter?.addToCart()
    }
    
    @IBAction func ShareAction(_ sender: Any) {
        presenter?.shareProduct()
    }
    @IBAction func favouriteProductAction(_ sender: Any) {
        presenter?.favouriteProduct()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollection()
        reloadFavouriteBtn()
        presenter?.ConfugurationProductDetails()
    } 
}
