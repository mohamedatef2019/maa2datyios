//
//  singleProductDetails+SingleProductDetailsView.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension SingleProductVC: singleProductDetailsView{
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func getPagesCount(count: Int) {
        pageControl.numberOfPages = count
    }
    
    func reloadCollection() {
        coll_OtherProducts.reloadData()
    }
    
    func reloadFavouriteBtn() {
        if presenter?.getProductFavourite() == 0{
            btn_Favourite.setImage(UIImage(named: "heart"), for: UIControl.State.normal)
        }else{
            btn_Favourite.setImage(UIImage(named: "like (-1"), for: UIControl.State.normal)
        }
    }
    
    func shareProduct(Link: String) {
        shareLink(url: Link)
    }
    
    func setProductTitle(title: String) {
        lbl_Title.text = title
    }
    
    func setProductRate(rate: Double) {
        view_currentRate.rating = rate
    }
    
    func setProductPrice(price: String) {
        lbl_Price.text = price
    }
    
    func setProductDescription(desc: String) {
        lbl_Description.text = desc
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

    
}
