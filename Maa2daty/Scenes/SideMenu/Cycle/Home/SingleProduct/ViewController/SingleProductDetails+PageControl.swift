//
//  SingleProductDetails+PageControl.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/11/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension SingleProductVC: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == coll_scrollingImages{
            let page = scrollView.contentOffset.x / scrollView.frame.size.width
            pageControl.currentPage = Int(page)
        }
    }
}
