//
//  SingleProductDetailsInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class SingleProductDetailsInteractor{
    func LoadProductDetails(ProductID: Int,completionHandler: @escaping (SingleProductDetailsResponse?, Error?) -> ()){
        APIManager.sharedInstance.getRequestWithLanguage("\(URLs.Products)\(ProductID)", headers: ["lang":SharedHandler.currentLanguage()]) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(SingleProductDetailsResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
    
    func SubmitRate(param: [String: Any],completionHandler: @escaping (DefaultResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequestWithHeader(URLs.setRate, header: ["lang":SharedHandler.currentLanguage()], Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(DefaultResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }

    func addProductToCart(param: [String: Any],completionHandler: @escaping (DefaultResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequestWithHeader(URLs.addToCart, header: ["lang":SharedHandler.currentLanguage(), "Content-Type":"application/json"], Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(DefaultResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
    
    func favouriteProduct(param: [String: Any],completionHandler: @escaping (DefaultResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequestWithHeader(URLs.favouriteProduct, header: ["lang":SharedHandler.currentLanguage(), "Content-Type":"application/json"], Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(DefaultResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
