//
//  SingleProductDetailsRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class SingleProductDetailsRouter{
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createProductsDetailsVC(response: SingleProductDetailsResponse, Title: String) -> UIViewController {
        
        let productsDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "SingleProductVC")
        if let view = productsDetailsVC as? singleProductDetailsView {
            let interactor = SingleProductDetailsInteractor()
            let router = SingleProductDetailsRouter()
            let presenter = SingleProductDetailsPresenter(view: view, interactor: interactor, router: router, res: response)
            view.presenter = presenter
        }
        return productsDetailsVC
    }
    
    func navigateToProductsDetailsScreen(from view: singleProductDetailsView?, response: SingleProductDetailsResponse, title: String) {
        let singleProduct = SingleProductDetailsRouter.createProductsDetailsVC(response: response, Title: title)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(singleProduct, animated: true)
        }
    }
}
