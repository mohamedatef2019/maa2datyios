//
//  SingleProductDetailsPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol singleProductDetailsView: class{
    
    var presenter: SingleProductDetailsPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func getPagesCount(count: Int)
    func setProductRate(rate: Double)
    func setProductTitle(title: String)
    func setProductPrice(price: String)
    func setProductDescription(desc: String)
    func shareProduct(Link: String)
    func reloadFavouriteBtn()
    func reloadCollection()
    func showMsg(message: String, image: String)
}

protocol imagesCellView {
    
    var presenter: SingleProductDetailsPresenter? { get set }
    func imageURL(url: String)
}

class SingleProductDetailsPresenter{
    private var singleProductResponse: SingleProductDetailsResponse
    private weak var view: singleProductDetailsView?
    private let interactor: SingleProductDetailsInteractor
    private let router: SingleProductDetailsRouter
    
    init(view: singleProductDetailsView, interactor: SingleProductDetailsInteractor, router: SingleProductDetailsRouter, res: SingleProductDetailsResponse){
        
        self.view = view
        self.interactor = interactor
        self.router = router
        self.singleProductResponse = res
    }
    
    func getIsFav() ->Int{
        return singleProductResponse.data.product.user_favorite
    }
    
    func getOtherProductCount() -> Int{
        return singleProductResponse.data.related_products.count
    }
    
    func getImagesForScrolling() -> Int{
        return singleProductResponse.data.product.product_images.count
    }
    
    func getPageCount(){
        view?.getPagesCount(count: singleProductResponse.data.product.product_images.count)
    }
    
    func getProductFavourite() -> Int{
        return singleProductResponse.data.product.user_favorite
    }
    
    func configureProducts(cell: ProductCell, for index: Int) {
        let SingleProduct = singleProductResponse.data.related_products[index]
        cell.SettingCosmosValue(Rate: Double(SingleProduct.rate ) ?? 0)
        cell.SettingProductName(Name: SingleProduct.name )
        cell.SettingProductImage(Image: SingleProduct.product_image)
        cell.SettingProductPrice(Price: String(SingleProduct.price))
        cell.SettingProductFavourite(isFav: Int(SingleProduct.user_favorite) ?? 0)
        cell.SettingProductDescription(Desc: SingleProduct.description)
        
        cell.btn_Cart.tag = index
        cell.btn_Share.tag = index
        cell.btn_isFavouriteProduct.tag = index
        
        cell.btn_Cart.addTarget(self, action: #selector(PressedCart(sender:)), for: UIControl.Event.touchUpInside)
        cell.btn_Share.addTarget(self, action: #selector(shareProduct(sender:)), for: UIControl.Event.touchUpInside)
        cell.btn_isFavouriteProduct.addTarget(self, action: #selector(setFavourite(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    func configureImages(index: Int, cell: ImagesCell){
        let singleImage = singleProductResponse.data.product.product_images[index]
        cell.imageURL(url: singleImage)
    }
    
    func LoadSingleProductDetails(index: Int){
        view?.showIndicator()
        interactor.LoadProductDetails(ProductID: singleProductResponse.data.related_products[index].id) { (res, error) in
            if error == nil && res != nil {
                self.router.navigateToProductsDetailsScreen(from: self.view, response: res!, title: self.singleProductResponse.data.related_products[index].name)
            }else{
                self.view?.showMsg(message: "Sorry, There is no item".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    func ConfugurationProductDetails() {
        view?.setProductTitle(title: singleProductResponse.data.product.name)
        view?.setProductPrice(price: String(singleProductResponse.data.product.price))
        view?.setProductRate(rate: Double(singleProductResponse.data.product.rate) ?? 0)
        view?.setProductDescription(desc: singleProductResponse.data.product.description)
    }
    
    func SubmitProductRate(rate: Double){
        view?.showIndicator()
        if SharedHandler.isLogged(){
            let param = [
                "jwt_token":SharedHandler.getUserJwt(),
                "user_id":SharedHandler.getUserID(),
                "product_id": singleProductResponse.data.product.id,
                "rate_value":rate
                ] as [String : Any]
            interactor.SubmitRate(param: param) { (res, error) in
                if error == nil{
                    if res?.status == 200{
                        self.view?.showMsg(message: res?.message ?? "", image: "like")
                    }else{
                        self.view?.showMsg(message: res?.message ?? "", image: "warning")
                    }
                }else{
                    self.view?.showMsg(message: "Please, Try Again Later".localized, image: "warning")
                }
                self.view?.hideIndicator()
            }
        }else{
            self.view?.showMsg(message: "Please, Login first".localized, image: "warning")
        }
    }
    
    func shareProduct(){
        view?.shareProduct(Link: singleProductResponse.data.product.share_link)
    }
    
    func addToCart(){
        view?.showIndicator()
        if SharedHandler.isLogged(){
            let param = [
                "user_id":"\(SharedHandler.getUserID())",
                "product_id":"\(singleProductResponse.data.product.id)",
                "qty":"1",
                "jwt_token":"\(SharedHandler.getUserJwt())"
            ]
            addToCart(param: param)
        }else{
            self.view?.showMsg(message: "Please, Login first".localized, image: "warning")
        }
    }
    
    func favouriteProduct(){
        
        if SharedHandler.isLogged(){
            let param = [
                "user_id":"\(SharedHandler.getUserID())",
                "product_id":"\(singleProductResponse.data.product.id)",
                "jwt_token":"\(SharedHandler.getUserJwt())"
            ]
            FavouriteProduct(param: param, isCell: false, index: 0)
        }else{
            self.view?.showMsg(message: "Please, Login first".localized, image: "warning")
        }
    }
    
    @objc func PressedCart(sender: UIButton){
        if SharedHandler.isLogged(){
            let param = [
                "user_id":SharedHandler.getUserID(),
                "product_id":singleProductResponse.data.related_products[sender.tag].id,
                "qty":"1",
                "jwt_token":"\(SharedHandler.getUserJwt())"
                ] as [String : Any]
            
            addToCart(param: param)
        }else{
            self.view?.showMsg(message: "Please, Login first".localized, image: "warning")
        }
        
    }
    
    @objc func setFavourite(sender: UIButton){
        if SharedHandler.isLogged(){
            let param = [
                "user_id":SharedHandler.getUserID(),
                "product_id":singleProductResponse.data.related_products[sender.tag].id,
                "jwt_token":SharedHandler.getUserJwt()
                ] as [String : Any]
            FavouriteProduct(param: param, isCell: true, index: sender.tag)
        }else{
            self.view?.showMsg(message: "Please, Login first".localized, image: "warning")
        }
    }
    
    @objc func shareProduct(sender: UIButton){
        view?.shareProduct(Link: singleProductResponse.data.related_products[sender.tag].share_link )
    }
    
    func addToCart(param: [String: Any]){
        view?.showIndicator()
        interactor.addProductToCart(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.view?.showMsg(message: res?.message ?? "", image: "like")
                }else{
                    self.view?.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Try Again Later".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
    
    func FavouriteProduct(param: [String: Any], isCell: Bool, index: Int){
        view?.showIndicator()
        interactor.favouriteProduct(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    if isCell{
                        if self.singleProductResponse.data.related_products[index].user_favorite == 0{
                            self.singleProductResponse.data.related_products[index].user_favorite = 1
                        }else{
                            self.singleProductResponse.data.related_products[index].user_favorite = 0
                        }
                        self.view?.reloadCollection()
                    }else{
                        if self.singleProductResponse.data.product.user_favorite == 0{
                            self.singleProductResponse.data.product.user_favorite = 1
                        }else{
                            self.singleProductResponse.data.product.user_favorite = 0
                        }
                        self.view?.reloadFavouriteBtn()
                    }
                    self.view?.showMsg(message: res?.message ?? "", image: "like")
                }else{
                    self.view?.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Try Again Later".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
}

