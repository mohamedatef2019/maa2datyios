//
//  OrderProductCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class OrderProductCell: UITableViewCell {

    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_Quantity: UILabel!
    @IBOutlet weak var view_Rate: CosmosView!
    @IBOutlet weak var lbl_Description: UILabel!
    
}

extension OrderProductCell: orderProductCellView{
    func settingImageProduct(image: String) {
        setImage(url: image, picture: img_Product)
    }
    
    func settingName(Name: String) {
        lbl_Name.text = Name
    }
    
    func settingPrice(Price: String) {
        lbl_Price.text = Price
    }
    
    func settingQuantity(Quantity: String) {
        lbl_Quantity.text = Quantity
    }
    
    func settingRate(Rate: Double) {
        view_Rate.rating = Rate
    }
    
    func settingDescription(Description: String) {
        lbl_Description.text = Description
    }
    
    
    
}
