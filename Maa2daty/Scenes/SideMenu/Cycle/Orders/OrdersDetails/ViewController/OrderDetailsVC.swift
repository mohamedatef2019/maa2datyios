//
//  OrderDetailsVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OrderDetailsVC: UIViewController {

    //MARK:- variable
    var presenter: OrderDetailsPresenter?
    
    //MARK:- outlets
    @IBOutlet weak var tbl_OrderDetailsVC: UITableView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter?.loadData()
        
    }

}
