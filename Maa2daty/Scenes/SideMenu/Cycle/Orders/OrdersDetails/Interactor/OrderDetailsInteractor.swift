//
//  OrderDetailsInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class OrderDetailsInteractor{
    func LoadData(orderID: Int,completionHandler: @escaping (OrderDetailsResponse?, Error?) -> ()){
        APIManager.sharedInstance.getRequestWithLanguage(URLs.orders + "/\(orderID)", headers: ["lang":SharedHandler.currentLanguage()]) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(OrderDetailsResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
