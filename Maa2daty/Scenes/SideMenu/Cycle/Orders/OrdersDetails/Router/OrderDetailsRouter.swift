//
//  OrderDetailsRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class OrderDetailsRouter{
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createOrderDetailsVC(orderID: Int) -> UIViewController {
        
        let OrderDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailsVC")
        if let view = OrderDetailsVC as? OrderDetailsView {
            let interactor = OrderDetailsInteractor()
            let presenter = OrderDetailsPresenter(view: view, interactor: interactor, router: OrderDetailsRouter(), orderID: orderID)
            view.presenter = presenter
        }
        return OrderDetailsVC
    }
}
