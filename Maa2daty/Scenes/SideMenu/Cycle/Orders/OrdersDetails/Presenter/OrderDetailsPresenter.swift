//
//  OrderDetailsPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol OrderDetailsView: class {
    var presenter: OrderDetailsPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchData()
    func showMsg(message: String, image: String)
}

protocol orderProductCellView {
    func settingImageProduct(image: String)
    func settingName(Name: String)
    func settingPrice(Price: String)
    func settingQuantity(Quantity: String)
    func settingRate(Rate: Double)
    func settingDescription(Description: String)
}


class OrderDetailsPresenter{
    private let orderID: Int
    private var arrOrderDetails: [OneOrderdetails] = []
    private let view: OrderDetailsView
    private let interactor: OrderDetailsInteractor
    private let router: OrderDetailsRouter
    
    init(view: OrderDetailsView, interactor: OrderDetailsInteractor,router: OrderDetailsRouter , orderID: Int) {
        self.view = view
        self.orderID = orderID
        self.interactor = interactor
        self.router = router
    }
    
    func loadData(){
        view.showIndicator()
        interactor.LoadData(orderID: orderID) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.arrOrderDetails = res?.orderDetails ?? []
                    self.view.fetchData()
                    if res?.orderDetails.count == 0{
                        SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                    }
                }else{
                    SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                    self.view.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                self.view.showMsg(message: res?.message ?? "", image: "warning")
            }
            self.view.hideIndicator()
        }
    }
    
    func getOrderscount() -> Int{
        return arrOrderDetails.count
    }
    
    func configurationCell(cell: OrderProductCell, index: Int){
        cell.settingName(Name: arrOrderDetails[index].product.name)
        cell.settingPrice(Price: "\(arrOrderDetails[index].product.price)")
        cell.settingQuantity(Quantity: "\(arrOrderDetails[index].qty)")
        cell.settingRate(Rate: Double(arrOrderDetails[index].product.rate) ?? 0.0)
        cell.settingImageProduct(image: arrOrderDetails[index].product.product_image)
        cell.settingDescription(Description: arrOrderDetails[index].product.description)
    }
}
