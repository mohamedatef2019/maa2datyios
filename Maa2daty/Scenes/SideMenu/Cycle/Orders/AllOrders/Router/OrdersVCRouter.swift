//
//  OrdersVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class OrdersVCRouter{
    func navigateOrdersDetailsScreen(from view: OrderVCView?, orderID: Int) {
        let orderDetailsView = OrderDetailsRouter.createOrderDetailsVC(orderID: orderID)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(orderDetailsView, animated: true)
        }
    }
}
