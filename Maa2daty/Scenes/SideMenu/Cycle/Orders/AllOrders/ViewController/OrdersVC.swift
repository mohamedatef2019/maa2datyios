//
//  OrdersVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OrdersVC: UIViewController {

    //MARK:- Variable
    var presenter: OrdersVCPresenter?
    
    
    //MARK:- Outlets
    @IBOutlet weak var tbl_Orders: UITableView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBTNs()
        let interactor = OrdersVCInteractor()
        let router = OrdersVCRouter()
        presenter = OrdersVCPresenter(view: self, interactor: interactor, router: router)
        presenter?.loadOrders()
        MenuRouter.CreateMenu(ViewController: self)
    }
}
