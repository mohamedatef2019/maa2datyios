//
//  OrderCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_PPrice: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_Number: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension OrderCell: OrderCellView{
    func settingDate(date: String) {
        lbl_Date.text = date
    }
    
    func settingPrice(price: String) {
        lbl_PPrice.text = price
    }
    
    func settingStatus(status: String) {
        lbl_Status.text = status
    }
    
    func settingNumber(number: String) {
        lbl_Number.text = number
    }
    
    
}
