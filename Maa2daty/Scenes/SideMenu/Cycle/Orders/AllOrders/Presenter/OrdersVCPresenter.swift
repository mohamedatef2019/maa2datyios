//
//  OrdersVCPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol OrderVCView: class {
    func showIndicator()
    func hideIndicator()
    func fetchData()
    func showMsg(message: String, image: String)
}
protocol OrderCellView {
    func settingDate(date: String)
    func settingPrice(price: String)
    func settingStatus(status: String)
    func settingNumber(number: String)
}
class OrdersVCPresenter{
    private var arrOrders: [Orders] = []
    private weak var interactor: OrdersVCInteractor?
    private let view : OrderVCView
    private let router: OrdersVCRouter
    
    init(view: OrderVCView, interactor: OrdersVCInteractor, router: OrdersVCRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadOrders(){
        view.showIndicator()
        interactor?.LoadData(completionHandler: { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.arrOrders = res?.data ?? []
                    self.view.fetchData()
                    if res?.data.count == 0{
                        SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                    }
                }else{
                    SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                    self.view.showMsg(message: "Sorry, There is no item".localized, image: "warning")
                }
            }else{
                SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                self.view.showMsg(message: "Please, Try Again Later".localized, image: "warning")
            }
            self.view.hideIndicator()
        })
    }
    
    func getOrdersCount() -> Int{
        return arrOrders.count
    }
    
    func configureCell(cell: OrderCell, index: Int){
        cell.settingDate(date: arrOrders[index].order_date)
        cell.settingPrice(price: arrOrders[index].total_fees)
        cell.settingNumber(number: arrOrders[index].order_number)
        cell.settingStatus(status: getStatusName(status: arrOrders[index].order_status))
    }
    
    func getStatusName(status: Int) -> String{
        switch status {
        case 0:
            return "unconfirmed order".localized
            
        case 1:
            return "new order".localized
            
        case 2:
            return "processing".localized
            
        case 3:
            return "delivered".localized
            
        default:
            return "unconfirmed order".localized
        }
    }
    
    func didSelectSingleOrder(index: Int){
        router.navigateOrdersDetailsScreen(from: view, orderID: arrOrders[index].id)
    }
}
