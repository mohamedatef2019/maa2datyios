//
//  OrdersVCInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation
import SwiftyJSON

class OrdersVCInteractor{
    func LoadData(completionHandler: @escaping (OrderResponse?, Error?) -> ()){
        APIManager.sharedInstance.getRequestWithLanguage(URLs.allOrder + "\(SharedHandler.getUserID())", headers: ["lang":SharedHandler.currentLanguage()]) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(OrderResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
