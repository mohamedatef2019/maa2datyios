//
//  ChangeLangaugeVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/12/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import MOLH

class changeLanguageVC: UIViewController {
    
    @IBAction func changeLang(_ sender: UIButton) {
        if sender.tag == 1 && SharedHandler.currentLanguage() == "en"{
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
            MOLH.reset()
        }else if sender.tag == 0 && SharedHandler.currentLanguage() == "ar"{
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
            MOLH.reset()
        }
        removeAnimation()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        removeAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
