//
//  ServicesSupporter+SevicesSupporterViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension ServicesSupporterVC: ServicesSupporterView{
    
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchData(index: Int) {
        tbl_Messages.reloadData()
        tbl_Messages.scrollToRow(at: IndexPath(row: index, section: 0), at: UITableView.ScrollPosition.none, animated: false)
    }
    
    func addingNewRow(index: Int) {
        tf_MessageText.text = ""
        tbl_Messages.insertRows(at: [IndexPath(item: index, section: 0)], with: UITableView.RowAnimation.bottom)
        tbl_Messages.scrollToRow(at: IndexPath(row: index, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
