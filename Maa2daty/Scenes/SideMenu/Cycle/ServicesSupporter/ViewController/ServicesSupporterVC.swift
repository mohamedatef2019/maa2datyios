//
//  ServicesSupporterVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ServicesSupporterVC: UIViewController {

    //MARK:- Variable
    var presenter: ServicesSupporterPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var tbl_Messages: UITableView!
    @IBOutlet weak var tf_MessageText: UITextField!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    
    //MARK:- Actions
    @IBAction func sendMessageAction(_ sender: Any) {
        presenter?.validationOnSendTextField(text: tf_MessageText.text!)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBTNs()

        presenter = ServicesSupporterPresenter(view: self, interactor: ServicesSupporterInteractor())
        presenter?.didLoad()
        
        MenuRouter.CreateMenu(ViewController: self)
    }
}
