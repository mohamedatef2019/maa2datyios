//
//  ServicesSupporter+TableDelagte.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension ServicesSupporterVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getMessagesCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if presenter?.getCellType(index: indexPath.row) ?? false {
            let cell = tbl_Messages.dequeueReusableCell(withIdentifier: "recieverCell") as! MessagesCell
            presenter?.configurationCell(cell: cell, index: indexPath.row)
            return cell
        }else{
            let cell = tbl_Messages.dequeueReusableCell(withIdentifier: "senderCell") as! MessagesCell
            presenter?.configurationCell(cell: cell, index: indexPath.row)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
