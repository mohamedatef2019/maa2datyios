//
//  ServicesSupporterPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol ServicesSupporterView {
    var presenter: ServicesSupporterPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchData(index: Int)
    func addingNewRow(index: Int)
    func showMsg(message: String, image: String)
}

protocol MessagesCellView{
    func settingMessageText(text: String)
    func settingProfileImage(image: String)
}

class ServicesSupporterPresenter{
    private var arrMessages: [Messages] = []
    private let view: ServicesSupporterView
    private let interactor: ServicesSupporterInteractor
    
    init(view: ServicesSupporterView, interactor: ServicesSupporterInteractor){
        self.view = view
        self.interactor = interactor
    }
    
    func didLoad(){
        view.showIndicator()
        interactor.LoadData { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.arrMessages = res?.messages ?? []
                    self.view.fetchData(index: self.arrMessages.count - 1)
                }else{
                    self.view.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
    
    func getMessagesCount() ->Int{
        return arrMessages.count
    }
    
    func getCellType(index: Int) -> Bool{
        return arrMessages[index].isAdmin
    }
    
    func configurationCell(cell: MessagesCell, index: Int){
        cell.settingMessageText(text: arrMessages[index].body)
        if getCellType(index: index){
            print("Default Image")
        }else{
            cell.settingProfileImage(image: SharedHandler.getuser_image())
        }
        
    }
    
    func validationOnSendTextField(text: String){
        if text.count > 0{
            sendMessage(text: text)
        }
    }
    
    private func sendMessage(text: String){
        let param = [
            "user_id":"\(SharedHandler.getUserID())",
            "body":text,
            "jwt_token":SharedHandler.getUserJwt(),
        ]
        view.showIndicator()
        interactor.sendMessages(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.arrMessages.append(Messages(body: text))
                    self.view.addingNewRow(index: self.arrMessages.count - 1)
                }
            }else{
                
            }
            self.view.hideIndicator()
        }
    }
}
