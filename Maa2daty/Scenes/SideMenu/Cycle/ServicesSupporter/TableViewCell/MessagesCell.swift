
//
//  MessagesCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class MessagesCell: UITableViewCell {

    @IBOutlet weak var lbl_MessageText: UILabel!
    @IBOutlet weak var img_PersonPrrofilePhoto: RoundImage!
    

}

extension MessagesCell: MessagesCellView{
    func settingMessageText(text: String) {
        lbl_MessageText.text = text
    }
    
    func settingProfileImage(image: String) {
        setImage(url: image, picture: img_PersonPrrofilePhoto)
    }
    
    
}

