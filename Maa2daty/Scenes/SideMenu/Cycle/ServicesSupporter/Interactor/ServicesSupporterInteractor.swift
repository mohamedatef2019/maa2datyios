//
//  ServicesSupporterInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class ServicesSupporterInteractor{
    func LoadData(completionHandler: @escaping (MessagesResponse?, Error?) -> ()){
        APIManager.sharedInstance.getRequest(URLs.allMessage + "\(SharedHandler.getUserID())") { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(MessagesResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
    
    func sendMessages(param: [String: Any],completionHandler: @escaping (DefaultResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequest(URLs.sendMessage, Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(DefaultResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
