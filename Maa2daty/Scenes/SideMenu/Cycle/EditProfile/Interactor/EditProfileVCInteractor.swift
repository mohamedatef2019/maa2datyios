//
//  EditProfileVCInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import SwiftyJSON
import Alamofire
import Foundation

class EditProfileVCInteractor{
    func EditProfileRequest(isSelectImage: Bool,selectedImage: UIImage,param: [String: String],completionHandler: @escaping (userResponse?, Error?) -> ()){
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if isSelectImage{
                multipartFormData.append(selectedImage.jpegData(compressionQuality: 1.0)!, withName: "user_image", fileName: "user_image.jpg", mimeType: "user_image/jpg")
            }
            
            for (key, value) in param {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
    }, usingThreshold: UInt64.init(), to: URLs.editProfile, method: HTTPMethod.post, headers: ["Content-Type":"application/json", "lang":SharedHandler.currentLanguage()]) { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    if response.error == nil{
                        do{
                            let Response = try JSONDecoder().decode(userResponse.self, from: response.data!)
                            completionHandler(Response, nil)
                        }catch{
                            completionHandler(nil, response.error)
                        }
                    }else{
                        completionHandler(nil, response.error)
                    }
                }
            case .failure:
                print("")
            }
        }
    }
}
