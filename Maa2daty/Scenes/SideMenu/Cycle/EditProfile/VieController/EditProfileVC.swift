
//
//  EditProfileVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class EditProfileVC: UIViewController {

    //MARK:- Variable
    var lat = "0.0"
    var lang = "0.0"
    var isSelectImage = false
    var selectedImages = UIImage()
    var presenter: EditProfileVCPresenter?
    let selectedImage = UIImagePickerController()
    
    
    //MARK:- Outlets
    @IBOutlet weak var tf_Email: UITextField!
    @IBOutlet weak var tf_Phone: UITextField!
    @IBOutlet weak var tf_LastName: UITextField!
    @IBOutlet weak var tf_Location: UITextField!
    @IBOutlet weak var tf_FirstName: UITextField!
    @IBOutlet weak var img_ProfileImage: RoundImage!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    
    //MARK:- Actions
    @IBAction func changeAction(_ sender: Any) {
        presenter?.validationOnFields(firstName: tf_FirstName, lastName: tf_LastName, phone: tf_Phone, mail: tf_Email, location: tf_Location, lat: lat, lang: lang, isSelectPhoto: isSelectImage, selectedImage: selectedImages)
    }
    
    @IBAction func selectProfileImageAction(_ sender: Any) {
        setupSelectedImage()
    }
    
    @IBAction func changePasswordVC(_ sender: Any) {
        presenter?.navigateToChangePassword()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        navigationBTNs()
        let interactor = EditProfileVCInteractor()
        let router = EditProfileVCRouter()
        presenter = EditProfileVCPresenter(view: self, interactor: interactor, router: router)
        
        MenuRouter.CreateMenu(ViewController: self)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshLocation), name: NSNotification.Name(rawValue: "refreshLocation"), object: nil)
    }
}
