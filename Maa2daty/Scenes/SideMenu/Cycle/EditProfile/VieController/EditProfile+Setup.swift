//
//  EditProfile+Setup.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

extension EditProfileVC{
    func setupView(){
        lat = SharedHandler.getlat()
        lang = SharedHandler.getlng()
        tf_Email.text = SharedHandler.getEmail()
        tf_Phone.text = SharedHandler.getUserPhone()
        tf_Location.text = SharedHandler.getlocation()
        tf_LastName.text = SharedHandler.getUserLastName()
        tf_FirstName.text = SharedHandler.getUserFirstName()
        setImage(url: SharedHandler.getuser_image(), picture: img_ProfileImage)
    }
}
