//
//  EditProfileVC+ImageDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Photos

extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func setupSelectedImage(){
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            selectedImage.delegate = self
            selectedImage.sourceType = UIImagePickerController.SourceType.photoLibrary
            selectedImage.allowsEditing = true
            self.present(selectedImage, animated: true)
        } else if (status == PHAuthorizationStatus.denied) {
        } else if (status == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    self.selectedImage.delegate = self
                    self.selectedImage.sourceType = UIImagePickerController.SourceType.photoLibrary
                    self.selectedImage.allowsEditing = true
                    self.present(self.selectedImage, animated: true)
                }
            })
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if picker == selectedImage{
            guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
                return
            }
            self.isSelectImage = true
            self.selectedImages = selectedImage
            self.img_ProfileImage.image = selectedImage
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
