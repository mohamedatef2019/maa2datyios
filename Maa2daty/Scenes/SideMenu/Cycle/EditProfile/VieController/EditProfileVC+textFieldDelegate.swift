//
//  EditProfileVC+textFieldDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation
extension EditProfileVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.endEditing(true)
        presenter?.navigateMapVC()
    }
    
    @objc func refreshLocation(notification: Notification){
        let data = notification.object as! mapVC
        self.tf_Location.text = data.geoLocation
        self.lat = data.latitude
        self.lang = data.longitude
    }
}
