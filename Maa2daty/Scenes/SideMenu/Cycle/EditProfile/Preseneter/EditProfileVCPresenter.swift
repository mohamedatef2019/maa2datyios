//
//  File.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol EditProfileView: class{
    var presenter: EditProfileVCPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchingDataSuccess()
    func showMsg(message: String, image: String)
}
class EditProfileVCPresenter{
    private weak var view: EditProfileView?
    private let interactor: EditProfileVCInteractor
    private let router: EditProfileVCRouter
    
    init(view: EditProfileView?, interactor: EditProfileVCInteractor, router: EditProfileVCRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func validationOnFields(firstName: UITextField, lastName: UITextField, phone: UITextField, mail: UITextField, location: UITextField, lat: String, lang: String, isSelectPhoto: Bool, selectedImage: UIImage){
        if SharedHandler.isValidAction(textFeilds: [firstName, lastName, phone, mail, location]){
            if phone.text!.isPhone(){
                if mail.text!.isValidEmail(testStr: mail.text!){
                    
                    let param = [
                        "user_id": "\(SharedHandler.getUserID())",
                        "first_name":firstName.text!,
                        "last_name":lastName.text!,
                        "phone":phone.text!,
                        "email":mail.text!,
                        "jwt_token": SharedHandler.getUserJwt(),
                        "location":location.text!,
                        "lat":lat,
                        "lng":lang,
                        "from":"ios"
                    ]
                    prepareForRegisteration(param: param, image: selectedImage, isSelectImage: isSelectPhoto)
                    
                }else{
                    self.view?.showMsg(message: "Please, Enter Valid Email".localized, image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Enter Valid Phone Number".localized, image: "warning")
            }
        }else{
            self.view?.showMsg(message: "All Fields is required".localized, image: "warning")
        }
    }
    
    func navigateMapVC(){
        router.navigateMapVC(from: view)
    }
    
    func navigateToChangePassword(){
        router.navigateChangePasswordVC(from: view, phone: SharedHandler.getUserPhone())
    }
    
    private func prepareForRegisteration(param: [String: String], image: UIImage, isSelectImage: Bool){
        view?.showIndicator()
        interactor.EditProfileRequest(isSelectImage: isSelectImage, selectedImage: image, param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    SharedHandler.settingUserDefauls(userData: res!.data)
                    self.view?.fetchingDataSuccess()
                    self.view?.showMsg(message: res?.message ?? "", image: "like")
                }else{
                    self.view?.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view?.hideIndicator()
        }
    }
}
