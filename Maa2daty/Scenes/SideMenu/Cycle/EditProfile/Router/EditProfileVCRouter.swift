//
//  EditProfileVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/17/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class EditProfileVCRouter{
    func navigateMapVC(from view: EditProfileView?) {
        let viewww = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapVC")
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(viewww, animated: true)
        }
    }
    
    func navigateChangePasswordVC(from view: EditProfileView?, phone: String) {
        let viewww = changePasswordRouter.createChangePasswordVC()
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(viewww, animated: true)
        }
    }
}
