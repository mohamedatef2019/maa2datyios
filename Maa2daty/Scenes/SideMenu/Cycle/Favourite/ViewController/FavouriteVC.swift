//
//  FavouriteVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class FavouriteVC: UIViewController {

    
    //MARK:- Variable
    var presenter: FavouriteVCPreseneter?
    
    //MARK:- Outlets
    @IBOutlet weak var coll_Favourite: UICollectionView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBTNs()
        setupCollectionView()
        let interactor = FavouriteVCInteractor()
        let router = FavouriteVCRouter()
        presenter = FavouriteVCPreseneter(view: self, interactor: interactor, router: router)
        presenter?.didLoad()
        MenuRouter.CreateMenu(ViewController: self)
    }
}
