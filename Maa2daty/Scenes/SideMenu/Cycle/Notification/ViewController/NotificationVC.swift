//
//  NotificationVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class NotificationVC: UIViewController {

    //MARK:- variable
    var presenter: NotificationPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tbl_Notification: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBTNs()
        presenter = NotificationPresenter(view: self, interactor: NotificationInteractor())
        presenter?.loadData()
        MenuRouter.CreateMenu(ViewController: self)
    }
}
