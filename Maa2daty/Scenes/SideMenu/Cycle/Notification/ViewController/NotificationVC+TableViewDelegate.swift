//
//  NotificationVC+TableViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension NotificationVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getNotificationCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_Notification.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        presenter?.configurationCell(cell: cell, index: indexPath.row)
        return cell
    }
    
    
}
