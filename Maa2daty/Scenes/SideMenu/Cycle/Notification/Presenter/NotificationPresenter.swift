//
//  NotificationPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol NotificationView: class {
    var presenter: NotificationPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchData()
    func showMsg(message: String, image: String)
}
protocol NotificationCellView {
    func settingImage(imge: String)
    func settingSince(since: String)
    func settingDesc(desc: String)
}
class NotificationPresenter{
    private var arrNotification: [Notifications] = []
    private let interactor: NotificationInteractor
    private let view : NotificationView
    
    init(view: NotificationView, interactor: NotificationInteractor) {
        self.view = view
        self.interactor = interactor
    }
    
    func loadData(){
        view.showIndicator()
        interactor.LoadData { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.arrNotification = res?.data ?? []
                    if res?.data.count == 0{
                        SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                    }else{
                        self.view.fetchData()
                    }
                }else{
                    SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                    self.view.showMsg(message: res?.message ?? "Please, Try again later".localized, image: "warning")
                }
            }else{
                SharedHandler.showNoDataView(ViewController: self.view as! UIViewController)
                self.view.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
    
    func getNotificationCount() ->Int{
        return arrNotification.count
    }
    
    func configurationCell(cell: NotificationCell, index: Int){
        cell.settingImage(imge: arrNotification[index].image)
        cell.settingSince(since: arrNotification[index].date)
        cell.settingDesc(desc: arrNotification[index].message)
    }
}
