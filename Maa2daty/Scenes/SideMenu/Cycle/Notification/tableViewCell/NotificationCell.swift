//
//  NotificationCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    
    @IBOutlet weak var img_Notification: RoundImage!
    @IBOutlet weak var lbl_Since: UILabel!
    @IBOutlet weak var lbl_Desc: UILabel!
    
}

extension NotificationCell: NotificationCellView{
    func settingImage(imge: String) {
        setImage(url: imge, picture: img_Notification)
    }
    
    func settingSince(since: String) {
        lbl_Since.text = since
    }
    
    func settingDesc(desc: String) {
        lbl_Desc.text = desc
    }
}

