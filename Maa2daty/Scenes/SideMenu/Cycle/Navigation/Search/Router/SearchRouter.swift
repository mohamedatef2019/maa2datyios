//
//  SearchRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class SearchRouter {
    func navigateToProductsDetailsScreen(from view: SearchView?, response: SingleProductDetailsResponse, title: String) {
        let singleProduct = SingleProductDetailsRouter.createProductsDetailsVC(response: response, Title: title)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(singleProduct, animated: true)
        }
    }
}
