//
//  SearchVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SearchVC: UIViewController {

    //MARK:- Variable
    var presenter: SearchPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var coll_products: UICollectionView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- Action
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupCollectionView()
        presenter = SearchPresenter(view: self, interactor: SearchInteractor(), router: SearchRouter())
    }
    

}
