//
//  Searcjvc+SearchView.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension SearchVC: SearchView{
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchDataSuccess() {
        coll_products.reloadData()
    }
    
    func shareProduct(Link: String) {
        shareLink(url: Link)
    }
    
    func reloadCollection() {
        coll_products.reloadData()
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
