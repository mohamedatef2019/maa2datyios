//
//  SearchVC+NavigationDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension SearchVC: UISearchBarDelegate{
    
    func setupNavigation(){
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 80, height: 20))
        searchBar.returnKeyType = .search
        searchBar.delegate = self
        searchBar.placeholder = "Search".localized
        let SearchButton = UIBarButtonItem(customView: searchBar)
        
        self.navigationItem.leftBarButtonItem = SearchButton
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
        if searchBar.text!.count > 0{
            presenter?.didLoad(searchText: searchBar.text!)
        }
    }
}
