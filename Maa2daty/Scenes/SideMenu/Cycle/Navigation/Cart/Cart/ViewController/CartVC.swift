//
//  CartVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CartVC: UIViewController {

    //MARK:- Variable
    var presenter: CartPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var tbl_CartItems: UITableView!
    @IBOutlet weak var lbl_TotalPrice: UILabel!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- Actions
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func completeOrderAction(_ sender: Any) {
        if Int(lbl_TotalPrice.text!) ?? 0 > 0{
            presenter?.completeOrderPressed(subTotal: lbl_TotalPrice.text!)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = CartPresenter(view: self, router: CartRouter(), interactor: CartInteractor())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter?.loadData()
    }
    

}
