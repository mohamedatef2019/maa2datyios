//
//  CartVC+tableDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension CartVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getCartCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_CartItems.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
        presenter?.configurationCell(cell: cell, index: indexPath.row)
        return cell
    }
    
}
