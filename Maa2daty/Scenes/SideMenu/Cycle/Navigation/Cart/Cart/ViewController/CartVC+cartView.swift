//
//  CartVC+cartView.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension CartVC: CartView{
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchData() {
        tbl_CartItems.reloadData()
    }
    
    func totalPrice(price: String) {
        lbl_TotalPrice.text = price
    }
    
    func deleteRow(index: Int) {
        self.tbl_CartItems.deleteRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.fade)
        self.perform(#selector(reload), with: self, afterDelay: 1.0)
    }
    
    @objc func reload(){
        tbl_CartItems.reloadData()
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
