//
//  CartInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class CartInteractor{
    func LoadData(completionHandler: @escaping (CartResponse?, Error?) -> ()){
        APIManager.sharedInstance.getRequestWithLanguage(URLs.cart + "\(SharedHandler.getUserID())", headers: ["lang":SharedHandler.currentLanguage()]) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(CartResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
    
    func DeleteCart(param: [String: Any],completionHandler: @escaping (DefaultResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequestWithHeader(URLs.deleteCart, header: ["lang":SharedHandler.currentLanguage(), "Content-Type":"application/json"], Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(DefaultResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
    
    func updateProductInCart(param: [String: Any],completionHandler: @escaping (DefaultResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequestWithHeader(URLs.addToCart, header: ["lang":SharedHandler.currentLanguage(), "Content-Type":"application/json"], Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(DefaultResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
