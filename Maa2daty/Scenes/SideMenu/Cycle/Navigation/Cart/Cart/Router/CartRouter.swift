//
//  CartRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class CartRouter{
    func navigateToRelatedProductID(from view: CartView?, cartID: Int) {
        let RelatedProducts = RelatedProductsRouter.createRelatedProductsVC(CartID: cartID)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(RelatedProducts, animated: true)
        }
    }
    
    func navigateToPaymentVC(from view: CartView?, subTotal: String) {
        let PaymentVC = PaymentVCRouter.createPaymentVC(SubTotal: subTotal)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(PaymentVC, animated: true)
        }
    }
}
