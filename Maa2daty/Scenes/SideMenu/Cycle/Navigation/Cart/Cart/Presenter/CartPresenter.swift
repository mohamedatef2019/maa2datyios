//
//  CartPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol CartView: class {
    var presenter: CartPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchData()
    func totalPrice(price: String)
    func deleteRow(index: Int)
    func showMsg(message: String, image: String)

}

protocol CartCellView{
    func settingImageProduct(image: String)
    func settingName(Name: String)
    func settingPrice(Price: String)
    func settingTotalCount(Count: String)
    func settingRate(Rate: Double)
    func settingDescription(Description: String)
    func settingFinishedCart(alpha: CGFloat)
}

class CartPresenter{
    private var arrCart: [Cart] = []
    private let view: CartView
    private let router: CartRouter
    private let interactor: CartInteractor
    
    init(view: CartView, router: CartRouter, interactor: CartInteractor){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadData(){
        self.view.showIndicator()
        interactor.LoadData { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.arrCart = res?.cart ?? []
                    self.view.fetchData()
                    self.sumCount()
                }else{
                    self.view.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view.showMsg(message: "Please, Try Again Later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
    
    func completeOrderPressed(subTotal: String){
        router.navigateToPaymentVC(from: view, subTotal: subTotal)
    }
    
    private func sumCount(){
        var sumTotal = 0
        for i in 0 ..< arrCart.count{
            if arrCart[i].is_out_of_stock == 0{
                sumTotal = sumTotal + (arrCart[i].product.price * arrCart[i].qty)
            }
        }
        view.totalPrice(price: "\(sumTotal)")
    }
    
    func getCartCount() ->Int{
        return arrCart.count
    }
    
    func configurationCell(cell: CartCell, index: Int){
        cell.settingName(Name: arrCart[index].product.name)
        cell.settingTotalCount(Count: "\(arrCart[index].qty)")
        cell.settingPrice(Price: "\(arrCart[index].product.price)")
        cell.settingRate(Rate: Double(arrCart[index].product.rate) ?? 0.0)
        cell.settingImageProduct(image: arrCart[index].product.product_image)
        cell.settingFinishedCart(alpha: CGFloat(arrCart[index].is_out_of_stock))
        cell.settingDescription(Description: arrCart[index].product.description)
        
        cell.btn_DeleteCart.tag = index
        cell.btn_DeleteCart.addTarget(self, action: #selector(deleteCartItem(sender:)), for: UIControl.Event.touchUpInside)
        
        cell.stepper.tag = index
        cell.stepper.value = Double(arrCart[index].qty)
        cell.stepper.addTarget(self, action: #selector(stepperValueChanged(sender:)), for: UIControl.Event.touchUpInside)
        
        cell.btn_Relateditems.tag = index
        cell.btn_Relateditems.addTarget(self, action: #selector(navigateToRelatedItem(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    @objc private func deleteCartItem(sender: UIButton){
        let data = arrCart[sender.tag]
        let param = [
            "user_id":"\(SharedHandler.getUserID())",
            "jwt_token":SharedHandler.getUserJwt(),
            "cart_item_id":"\(data.cart_item_id)"
        ]
        deleteCart(param: param, index: sender.tag)
    }
    
    @objc func stepperValueChanged(sender: UIStepper){
        print(arrCart[sender.tag].qty)
        print(sender.value)
        if arrCart[sender.tag].qty >= 1 && sender.value > 0{
            var isPlus = 0
            if arrCart[sender.tag].qty < Int(sender.value){
                isPlus = 1
            }
            
            updateCart(index: sender.tag, plus: isPlus, value: Int(sender.value))
            
        }else{
            let data = arrCart[sender.tag]
            let param = [
                "user_id":"\(SharedHandler.getUserID())",
                "jwt_token":SharedHandler.getUserJwt(),
                "cart_item_id":"\(data.cart_item_id)"
            ]
            deleteCart(param: param, index: sender.tag)
        }
    }
    
    private func deleteCart(param: [String: String], index: Int){
        
        self.view.showIndicator()
        interactor.DeleteCart(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.arrCart.remove(at: index)
                    self.sumCount()
                    self.view.deleteRow(index: index)
                }else{
                    self.view.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view.showMsg(message: "Please, Try Again Later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
    
    private func updateCart(index: Int, plus: Int, value: Int){
        let param = [
            "user_id":"\(SharedHandler.getUserID())",
            "product_id":"\(arrCart[index].product.id)",
            "plus":"\(plus)",
            "jwt_token":"\(SharedHandler.getUserJwt())",
            "cart_item_id":"\(arrCart[index].cart_item_id)"
            ] as [String : Any]
        view.showIndicator()
        interactor.updateProductInCart(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.arrCart[index].qty = value
                    self.sumCount()
                    self.view.fetchData()
                }
                self.view.showMsg(message: res?.message ?? "", image: "warning")
            }
            self.view.hideIndicator()
        }
    }
    
    @objc private func navigateToRelatedItem(sender: UIButton){
        router.navigateToRelatedProductID(from: view, cartID: arrCart[sender.tag].cart_item_id)
    }
}
