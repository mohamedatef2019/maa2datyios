//
//  CartCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/19/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var totalCount: UILabel!
    @IBOutlet weak var view_Rate: CosmosView!
    @IBOutlet weak var lbl_Description: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var btn_DeleteCart: UIButton!
    @IBOutlet weak var btn_Relateditems: UIButton!
    @IBOutlet weak var view_FinishedCart: customView!
    
}

extension CartCell: CartCellView{
    func settingImageProduct(image: String) {
        setImage(url: image, picture: img_Product)
    }
    
    func settingName(Name: String) {
        lbl_Name.text = Name
    }
    
    func settingPrice(Price: String) {
        lbl_Price.text = Price
    }
    
    func settingTotalCount(Count: String) {
        totalCount.text = Count
    }
    
    func settingRate(Rate: Double) {
        view_Rate.rating = Rate
    }
    
    func settingDescription(Description: String) {
        lbl_Description.text = Description
    }
    
    func settingFinishedCart(alpha: CGFloat) {
        view_FinishedCart.alpha = alpha
    }
    
    
}


