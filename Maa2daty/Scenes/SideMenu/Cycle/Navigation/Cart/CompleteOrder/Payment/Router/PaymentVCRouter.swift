//
//  PaymentVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class PaymentVCRouter{
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createPaymentVC(SubTotal: String) -> UIViewController {
        
        let PaymentVC = mainStoryboard.instantiateViewController(withIdentifier: "PaymentVC")
        if let view = PaymentVC as? PaymentView {
            let interactor = PaymentVCInteractor()
            let router = PaymentVCRouter()
            let presenter = PaymentVCPresenter(view: view, router: router, interactor: interactor, subTotal: SubTotal)
            view.presenter = presenter
        }
        return PaymentVC
    }
    
    func navigateToAddressVC(from view: PaymentView?, arrAddress: [Address_list]) {
        let AddressVC = AddressVCRouter.createAddressVC(arrAddress: arrAddress)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(AddressVC, animated: true)
        }
    }
}
