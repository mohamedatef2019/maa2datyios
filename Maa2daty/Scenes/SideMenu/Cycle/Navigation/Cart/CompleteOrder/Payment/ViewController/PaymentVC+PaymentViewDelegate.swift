//
//  PaymentVC+PaymentViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension PaymentVC: PaymentView{
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func settingName(name: String) {
        lbl_Name.text = name
    }
    
    func settingPhone(Phone: String) {
        lbl_Phone.text = Phone
    }
    
    func settingTotal(Total: String) {
        lbl_Total.text = Total
    }
    
    func settingPoints(Points: String) {
        lbl_Points.text = Points
    }
    
    func settingAddress(Address: String) {
        lbl_Address.text = Address
    }
    
    func settingShipping(Shipping: String) {
        lbl_Shipping.text = Shipping
    }
    
    func settingSubTotal(SubTotal: String) {
        lbl_SubTotal.text = SubTotal
    }
    
    func settingAddressView(hidden: Bool) {
        view_address.isHidden = hidden
    }
    
    func closeSwitch() {
        pointsSwitch.isOn = false
    }
    
    func dismissCycle() {
        perform(#selector(dismissView), with: self, afterDelay: 2.0)
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
