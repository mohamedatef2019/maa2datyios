//
//  PaymentVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PaymentVC: UIViewController {

    //MARK:- Variable
    var ordertimePicker = UIDatePicker()
    var orderDatePicker = UIDatePicker()
    var presenter: PaymentVCPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var lbl_Points: UILabel!
    @IBOutlet weak var lbl_SubTotal: UILabel!
    @IBOutlet weak var lbl_Shipping: UILabel!
    @IBOutlet weak var lbl_Total: UILabel!
    
    @IBOutlet weak var view_address: UIView!
    
    @IBOutlet weak var tf_Vouchercode: UITextField!
    @IBOutlet weak var tf_Date: UITextField!
    @IBOutlet weak var tf_Time: UITextField!
    @IBOutlet weak var pointsSwitch: UISwitch!
    
    //MARK:- Actions
    @IBAction func editAddressAction(_ sender: Any) {
        presenter?.editAddressPressed()
    }
    
    @IBAction func applyVoucherCodeAction(_ sender: Any) {
        presenter?.applyCodeAction(code: tf_Vouchercode.text!)
    }
    
    @IBAction func selectPaymentTypeActions(_ sender: UIButton) {
        /*
         sender.tag == 1  -> cash
         sender.tag == 2  -> Online
         */
    }
    
    @IBAction func pointsAction(_ sender: UISwitch) {
        var isPoint = 0
        if sender.isOn{
            isPoint = 1
        }
        presenter?.pointsAction(willUsePoint: isPoint)
    }
    
    @IBAction func proccessedPaymentAction(_ sender: Any) {
        presenter?.validationForSubtmitOrder(time: tf_Time, Date: tf_Date)
    }
    
    //MARK:- appCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        showDatePicker()
        openTimePicker()
        tf_Time.inputView = ordertimePicker
        tf_Date.inputView = orderDatePicker
        presenter?.didLoad()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "selectAddress"), object: nil, queue: nil, using: presenter!.catchNotification)
    }
}
