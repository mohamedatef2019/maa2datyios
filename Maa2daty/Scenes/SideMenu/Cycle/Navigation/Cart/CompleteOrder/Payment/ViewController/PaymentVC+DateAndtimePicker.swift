//
//  PaymentVC+DateAndtimePicker.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/26/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension PaymentVC{
    func showDatePicker(){
        orderDatePicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .done, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tf_Date.inputAccessoryView = toolbar
        
    }
    func openTimePicker()  {
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        ordertimePicker.datePickerMode = .time
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(startTimeDiveChanged))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .done, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tf_Time.inputAccessoryView = toolbar

    }
    
    @objc func startTimeDiveChanged() {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        tf_Time.text = formatter.string(from: ordertimePicker.date)
        ordertimePicker.removeFromSuperview()
        self.view.endEditing(true)
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        tf_Date.text = formatter.string(from: orderDatePicker.date)
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
