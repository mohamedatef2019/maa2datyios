//
//  File.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class PaymentVCInteractor{
    func LoadData(completionHandler: @escaping (OrderSummaryResponse?, Error?) -> ()){
        APIManager.sharedInstance.getRequest(URLs.orderSummary + "\(SharedHandler.getUserID())") { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(OrderSummaryResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
    
    func redeemPoints(param: [String: String],completionHandler: @escaping (RedeemPointsResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequest(URLs.redeemPoints, Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(RedeemPointsResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
    
    func promoCode(param: [String: String],completionHandler: @escaping (PromotionCodeResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequest(URLs.promoCode, Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(PromotionCodeResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
    
    func submitOrder(param: [String: String],completionHandler: @escaping (DefaultResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequest(URLs.orders, Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(DefaultResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
