//
//  PaymentVCPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation


protocol PaymentView: class{
    var presenter: PaymentVCPresenter? { get set }
    func showIndicator()
    func hideIndicator()
    func closeSwitch()
    func dismissCycle()
    func showMsg(message: String, image: String)
    
    func settingName(name: String)
    func settingPhone(Phone: String)
    func settingTotal(Total: String)
    func settingPoints(Points: String)
    func settingAddress(Address: String)
    func settingShipping(Shipping: String)
    func settingSubTotal(SubTotal: String)
    func settingAddressView(hidden: Bool)
}

class PaymentVCPresenter{
    private var orderID = 0
    private var points = ""
    private var date = ""
    private var time = ""
    private var selectedAddressID = 0
    private var arrAddress: [Address_list] = []
    private let subTotal: String
    private let view: PaymentView
    private let router: PaymentVCRouter
    private let interactor: PaymentVCInteractor
    
    
    init(view: PaymentView, router: PaymentVCRouter, interactor: PaymentVCInteractor, subTotal: String) {
        self.view = view
        self.router = router
        self.subTotal = subTotal
        self.interactor = interactor
    }
    
    func didLoad(){
        self.view.showIndicator()
        interactor.LoadData { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.points = res?.data?.user_points ?? ""
                    self.orderID = res?.data?.order_id ?? 0
                    self.arrAddress = res?.data?.address_list ?? []
                    if res?.data?.address_list.count ?? 0 > 0{
                        self.selectedAddressID = res?.data?.address_list[0].id ?? 0
                        self.view.settingAddressView(hidden: false)
                        self.view.settingName(name: res?.data?.address_list[0].first_name ?? "" + " " + (res?.data?.address_list[0].last_name)!)
                        self.view.settingPhone(Phone: res?.data?.address_list[0].phone ?? "")
                        self.view.settingAddress(Address: res?.data?.address_list[0].address ?? "")
                    }else{
                        self.view.settingAddressView(hidden: true)
                    }
                    self.view.settingPoints(Points: res?.data?.user_points ?? "")
                    self.view.settingShipping(Shipping: res?.data?.shipping ?? "")
                    self.view.settingSubTotal(SubTotal: self.subTotal)
                    self.view.settingTotal(Total: "\(Int(self.subTotal)! + Int(res?.data?.shipping ?? "0")!)")
                }else{
                    self.view.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
    
    func editAddressPressed(){
        router.navigateToAddressVC(from: view, arrAddress: arrAddress)
    }
    
    private func changeAddress(index: Int){
        self.view.settingName(name: arrAddress[index].first_name + " " + arrAddress[index].last_name)
        self.view.settingPhone(Phone: arrAddress[index].phone)
        self.view.settingAddress(Address: arrAddress[index].address)
    }
    
    func catchNotification(notification:Notification) -> Void {
        guard let arrAddress = notification.userInfo!["arrAddress"] else { return }
        guard let index = notification.userInfo!["index"] else { return }
        
        self.arrAddress = arrAddress as! [Address_list]
        self.selectedAddressID = self.arrAddress[index as! Int].id
        changeAddress(index: index as! Int)
        view.settingAddressView(hidden: true)
    }
    
    func pointsAction(willUsePoint: Int){
        if points != "" {
            pointsRequest(willUsePoint: willUsePoint)
        }else{
            view.closeSwitch()
        }
        
    }
    
    private func pointsRequest(willUsePoint: Int){
        let param = [
            "user_id":"\(SharedHandler.getUserID())",
            "redeem":"\(willUsePoint)",
            "order_id":"\(orderID)",
            "jwt_token":SharedHandler.getUserJwt()
        ]
        print(param)
        self.view.showIndicator()
        interactor.redeemPoints(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.view.settingTotal(Total: "\(res!.data.total_fees)")
                }else{
                    self.view.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
    
    func applyCodeAction(code: String){
        if code != ""{
            applyPromoCodeRequest(code: code)
        }
    }
    
    private func applyPromoCodeRequest(code: String){
        let param = [
            "user_id":"\(SharedHandler.getUserID())",
            "order_id":"\(orderID)",
            "promo_code":code
        ]
        self.view.showIndicator()
        interactor.promoCode(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.view.settingTotal(Total: "\(res!.data.discounted_price.total_fees)")
                    self.view.settingSubTotal(SubTotal: "\(res!.data.discounted_price.new_subtotal)")
                }else{
                    self.view.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
    
    func validationForSubtmitOrder(time: UITextField, Date: UITextField){
        if SharedHandler.isValidAction(textFeilds: [time, Date]){
            if selectedAddressID > 0{
                proccessOrderAction(time: time.text!, Date: Date.text!)
            }else{
                self.view.showMsg(message: "Please, Enter Your Address".localized, image: "warning")
            }
        }else{
            self.view.showMsg(message: "All Fields is required".localized, image: "warning")
        }
    }
    private func proccessOrderAction(time: String, Date: String){
        let param = [
            "user_id":"\(SharedHandler.getUserID())",
            "jwt_token":SharedHandler.getUserJwt(),
            "order_time":time,
            "order_date":Date,
            "payment_method":"1",
            "delivery_address_id":"\(selectedAddressID)",
            "order_id":"\(orderID)"
        ]
        self.view.showIndicator()
        self.interactor.submitOrder(param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.view.dismissCycle()
                    self.view.showMsg(message: res?.message ?? "", image: "like")
                }else{
                    self.view.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
}
