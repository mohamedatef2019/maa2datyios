//
//  AddressVC+tableDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension AddressVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (presenter?.getAddressCount() ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tbl_Address.dequeueReusableCell(withIdentifier: "newAddressCell") as! newAddressCell
            presenter?.configureAddAddress(cell: cell)
            return cell
        }else{
            let cell = tbl_Address.dequeueReusableCell(withIdentifier: "userAddressCell") as! userAddressCell
            presenter?.configureUserAddressCell(cell: cell, index: indexPath.row - 1)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
