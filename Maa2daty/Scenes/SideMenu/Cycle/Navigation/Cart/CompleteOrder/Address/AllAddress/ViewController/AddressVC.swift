//
//  AddressVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class AddressVC: UIViewController {

    
    //MARK:- Variable
    var presenter: AddressVCPreseneter?
    
    //MARK:- Outlets
    @IBOutlet weak var tbl_Address: UITableView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "reloadAddress"), object: nil, queue: nil, using: presenter!.catchNotification)
    }

}
