//
//  userAddressCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class userAddressCell: UITableViewCell, userAddressCellView {
    
    
    
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_phone: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var btn_selectAddress: UIButton!
    @IBOutlet weak var btn_Edit: UIButton!
    
    func settingName(name: String) {
        lbl_Name.text = name
    }
    
    func settingPhone(phone: String) {
        lbl_phone.text = phone
    }
    
    func settingAddress(address: String) {
        lbl_Address.text = address
    }
}
