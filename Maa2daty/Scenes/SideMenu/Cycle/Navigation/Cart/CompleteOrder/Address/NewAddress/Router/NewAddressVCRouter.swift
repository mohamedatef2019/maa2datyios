//
//  AddressVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class NewAddressVCRouter{
    
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createNewAddress() -> UIViewController {
        
        let NewAddressVC = mainStoryboard.instantiateViewController(withIdentifier: "NewAddressVC")
        if let view = NewAddressVC as? NewAddressView {
            let interactor = NewAddressVCInteractor()
            let router = NewAddressVCRouter()
            let presenter = NewAddressPresenter(view: view, router: router, interactor: interactor)
            view.presenter = presenter
        }
        return NewAddressVC
    }
    
    class func editAddress(singleAddress: Address_list) -> UIViewController {
        
        let NewAddressVC = mainStoryboard.instantiateViewController(withIdentifier: "NewAddressVC")
        if let view = NewAddressVC as? NewAddressView {
            let interactor = NewAddressVCInteractor()
            let router = NewAddressVCRouter()
            let presenter = NewAddressPresenter(view: view, router: router, interactor: interactor, singleaddress: singleAddress)
            view.presenter = presenter
        }
        return NewAddressVC
    }
    
    func navigateMapVC(from view: NewAddressView?) {
        let viewww = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapVC")
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(viewww, animated: true)
        }
    }
    
    func navigationPopup(from view: NewAddressView?){
        if let viewController = view as? UIViewController {
            viewController.navigationController?.popViewController(animated: true)
        }
    }
}
