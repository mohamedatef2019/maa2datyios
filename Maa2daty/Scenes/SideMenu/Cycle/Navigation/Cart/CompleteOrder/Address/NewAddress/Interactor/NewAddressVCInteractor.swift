//
//  AddressVCInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class NewAddressVCInteractor{
    func editAddress(url: String,param: [String: Any],completionHandler: @escaping (DefaultResponse?, Error?) -> ()){
        APIManager.sharedInstance.postRequestWithHeader(url, header: ["lang":SharedHandler.currentLanguage(), "Content-Type":"application/json"], Parameters: param) { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(DefaultResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
