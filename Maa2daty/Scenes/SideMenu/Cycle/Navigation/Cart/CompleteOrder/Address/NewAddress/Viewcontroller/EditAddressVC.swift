//
//  EditAddressVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class EditAddressVC: UIViewController {

    
    //MARK:- Variable
    var lat = "0.0"
    var lang = "0.0"
    var presenter: NewAddressPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var tf_FirstName: UITextField!
    @IBOutlet weak var tf_LastName: UITextField!
    @IBOutlet weak var tf_Phone: UITextField!
    @IBOutlet weak var tf_Address: UITextField!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- Actions
    @IBAction func SaveAction(_ sender: Any) {
        presenter?.savePressed(firstName: tf_FirstName, lastName: tf_LastName, phone: tf_Phone, address: tf_Address, lat: lat, long: lang)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.didload()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshLocation), name: NSNotification.Name(rawValue: "refreshLocation"), object: nil)
    }
    
}
