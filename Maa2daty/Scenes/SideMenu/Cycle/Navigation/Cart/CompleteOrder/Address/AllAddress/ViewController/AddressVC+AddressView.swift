//
//  AddressVC+AddressView.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension AddressVC: AddressView{
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchData() {
        tbl_Address.reloadData()
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

}
