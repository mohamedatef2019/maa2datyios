//
//  AddressVCRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class AddressVCRouter{
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createAddressVC(arrAddress: [Address_list]) -> UIViewController {
        
        let AddressVC = mainStoryboard.instantiateViewController(withIdentifier: "AddressVC")
        if let view = AddressVC as? AddressView {
            let interactor = AddressVCInteractor()
            let router = AddressVCRouter()
            let presenter = AddressVCPreseneter(view: view, router: router, interactor: interactor, arrAddress: arrAddress)
            view.presenter = presenter
        }
        return AddressVC
    }
    
    func navigateToNewAddressVC(from view: AddressView?) {
        let AddressVC = NewAddressVCRouter.createNewAddress()
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(AddressVC, animated: true)
        }
    }
    
    func navigateToEditAddressVC(from view: AddressView?, singleAddress: Address_list) {
        let AddressVC = NewAddressVCRouter.editAddress(singleAddress: singleAddress)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(AddressVC, animated: true)
        }
    }
    
    func navigationPopup(from view: AddressView?){
        if let viewController = view as? UIViewController {
            viewController.navigationController?.popViewController(animated: true)
        }
    }
}
