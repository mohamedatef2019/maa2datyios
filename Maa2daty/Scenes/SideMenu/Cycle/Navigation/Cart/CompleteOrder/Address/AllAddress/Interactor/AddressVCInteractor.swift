//
//  AddressVCInteractor.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class AddressVCInteractor{
    func LoadData(completionHandler: @escaping (AllAddressResponse?, Error?) -> ()){
        APIManager.sharedInstance.getRequest(URLs.addNewAddress + "/\(SharedHandler.getUserID())") { (res) in
            if res.error == nil{
                do{
                    let Response = try JSONDecoder().decode(AllAddressResponse.self, from: res.data!)
                    completionHandler(Response, nil)
                }catch{
                    completionHandler(nil, res.error)
                }
            }else{
                completionHandler(nil, res.error)
            }
        }
    }
}
