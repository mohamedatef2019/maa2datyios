//
//  NewAddressPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol NewAddressView: class{
    var presenter: NewAddressPresenter? { get set }
    func settingLastName(LastName: String)
    func settingFirst(name: String)
    func settingPhone(phone: String)
    func settingAddress(address: String)
    func settinglatLong(lat: String, Long: String)
    func showIndicator()
    func hideIndicator()
    func showMsg(message: String, image: String)
}

class NewAddressPresenter{
    private var singleAddres: Address_list = Address_list()
    private let urlForRequest: String
    private let view: NewAddressView
    private let router: NewAddressVCRouter
    private let interactor: NewAddressVCInteractor
    
    
    init(view: NewAddressView, router: NewAddressVCRouter, interactor: NewAddressVCInteractor) {
        self.view = view
        self.router = router
        self.interactor = interactor
        self.urlForRequest = URLs.addNewAddress
    }
    
    init(view: NewAddressView, router: NewAddressVCRouter, interactor: NewAddressVCInteractor, singleaddress: Address_list) {
        self.view = view
        self.router = router
        self.interactor = interactor
        self.urlForRequest = URLs.editAddress
        self.singleAddres = singleaddress
    }
    
    func didload(){
        self.view.settingFirst(name: singleAddres.first_name)
        self.view.settingPhone(phone: singleAddres.phone)
        self.view.settingAddress(address: singleAddres.address)
        self.view.settingLastName(LastName: singleAddres.last_name)
        self.view.settinglatLong(lat: singleAddres.lat, Long: singleAddres.lng)
    }
    
    func navigateMapVC(){
        router.navigateMapVC(from: view)
    }
    
    func savePressed(firstName: UITextField, lastName: UITextField, phone: UITextField, address: UITextField, lat: String, long: String){
        if SharedHandler.isValidAction(textFeilds: [firstName, lastName, phone, address]) {
            saveRequest(firstName: firstName, lastName: lastName, phone: phone, address: address, lat: lat, long: long)
        } else {
            self.view.showMsg(message: "All Fields is required".localized, image: "warning")
        }
    }
    
    private func saveRequest(firstName: UITextField, lastName: UITextField, phone: UITextField, address: UITextField, lat: String, long: String){
        let param = [
            "first_name":firstName.text!,
            "last_name":lastName.text!,
            "phone":phone.text!,
            "address":address.text!,
            "address_id":"\(singleAddres.id)",
            "user_id":"\(SharedHandler.getUserID())",
            "jwt_token":SharedHandler.getUserJwt(),
            "lat":lat,
            "lng":long
        ]
        self.view.showIndicator()
        interactor.editAddress(url: urlForRequest, param: param) { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.view.showMsg(message: res?.message ?? "", image: "like")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAddress"), object: nil, userInfo: nil)
                    self.router.navigationPopup(from: self.view)
                }else{
                    self.view.showMsg(message: res?.message ?? "", image: "waring")
                }
            }else{
                self.view.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
}
