//
//  NewAddress+textFieldDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension EditAddressVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.endEditing(true)
        presenter?.navigateMapVC()
    }
    
    @objc func refreshLocation(notification: Notification){
        let data = notification.object as! mapVC
        self.tf_Address.text = data.geoLocation
        self.lat = data.latitude
        self.lang = data.longitude
    }
}
