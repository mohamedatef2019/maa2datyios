//
//  AddressVCPreseneter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol AddressView: class{
    var presenter: AddressVCPreseneter? { get set }
    func showIndicator()
    func hideIndicator()
    func fetchData()
    func showMsg(message: String, image: String)
}

protocol userAddressCellView{
    func settingName(name: String)
    func settingPhone(phone: String)
    func settingAddress(address: String)
}

class AddressVCPreseneter{
    private var arrAddress: [Address_list]
    private let view: AddressView
    private let router: AddressVCRouter
    private let interactor: AddressVCInteractor
    
    
    init(view: AddressView, router: AddressVCRouter, interactor: AddressVCInteractor, arrAddress: [Address_list]) {
        self.view = view
        self.router = router
        self.arrAddress = arrAddress
        self.interactor = interactor
    }
    
    func getAddressCount() ->Int{
        return arrAddress.count
    }
    
    func configureAddAddress(cell: newAddressCell){
        cell.addNewAddress.addTarget(self, action: #selector(navigateToAddNewAddress), for: UIControl.Event.touchUpInside)
    }
    
    func configureUserAddressCell(cell: userAddressCell, index: Int){
        cell.settingName(name: arrAddress[index].first_name + " " + arrAddress[index].last_name)
        cell.settingAddress(address: arrAddress[index].address)
        cell.settingPhone(phone: arrAddress[index].phone)
        
        cell.btn_Edit.tag = index
        cell.btn_Edit.addTarget(self, action: #selector(navigateToAddEditAddress(sender:)), for: UIControl.Event.touchUpInside)
        
        cell.btn_selectAddress.tag = index
        cell.btn_selectAddress.addTarget(self, action: #selector(selectAddress(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    @objc private func navigateToAddNewAddress(){
        router.navigateToNewAddressVC(from: view)
    }
    
    @objc private func navigateToAddEditAddress(sender: UIButton){
        router.navigateToEditAddressVC(from: view, singleAddress: arrAddress[sender.tag])
    }
    
    @objc private func selectAddress(sender: UIButton){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectAddress"), object: nil, userInfo: ["index":sender.tag, "arrAddress": arrAddress])
        router.navigationPopup(from: view)
    }
    
    func catchNotification(notification:Notification) -> Void {
        didLoad()
    }
    
    func didLoad(){
        self.view.showIndicator()
        interactor.LoadData { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.arrAddress = res?.data ?? []
                    self.view.fetchData()
                }else{
                    self.view.showMsg(message: res?.message ?? "", image: "warning")
                }
            }else{
                self.view.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view.hideIndicator()
        }
    }
}

