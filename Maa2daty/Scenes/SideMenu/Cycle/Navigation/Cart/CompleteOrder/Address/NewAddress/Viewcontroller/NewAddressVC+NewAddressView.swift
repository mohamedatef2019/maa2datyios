//
//  NewAddressVC+NewAddressView.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/21/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension EditAddressVC: NewAddressView{
    func settinglatLong(lat: String, Long: String) {
        self.lat = lat
        self.lang = Long
        
    }
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
    
    
    
    func settingLastName(LastName: String) {
        tf_LastName.text = LastName
    }
    
    func settingFirst(name: String) {
        tf_FirstName.text = name
    }
    
    func settingPhone(phone: String) {
        tf_Phone.text = phone
    }
    
    func settingAddress(address: String) {
        tf_Address.text = address
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

    
}
