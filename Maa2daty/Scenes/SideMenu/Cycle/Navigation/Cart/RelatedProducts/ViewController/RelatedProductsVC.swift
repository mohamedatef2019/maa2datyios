//
//  RelatedProductsVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/20/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class RelatedProductsVC: UIViewController {

    //MARK:- Variable
    var presenter: RelatedProductsPresenter?
    
    //MARK:- Outlets
    @IBOutlet weak var coll_Products: UICollectionView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        presenter?.didLoad()
    }

}
