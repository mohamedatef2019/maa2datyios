//
//  RelatedProducts+CollectionDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/20/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension RelatedProductsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func setupCollectionView() {
        coll_Products.dataSource = self
        coll_Products.delegate = self
        coll_Products.reloadData()
        coll_Products.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.getProductsCount() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = coll_Products.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        presenter?.configure(cell: cell, for: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.view.frame.width > 450{
            return CGSize(width: (self.view.frame.width / 3), height: 295.0)
        }else{
            return CGSize(width: (self.view.frame.width / 2), height: 295.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.LoadSingleProductDetails(index: indexPath.row)
    }
    
}
