//
//  RelatedProductsRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/20/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

class RelatedProductsRouter{
    func navigateToProductsDetailsScreen(from view: RelatedProductsView?, response: SingleProductDetailsResponse, title: String) {
        let singleProduct = SingleProductDetailsRouter.createProductsDetailsVC(response: response, Title: title)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(singleProduct, animated: true)
        }
    }
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func createRelatedProductsVC(CartID: Int) -> UIViewController {
        
        let RelatedProductsVC = mainStoryboard.instantiateViewController(withIdentifier: "RelatedProductsVC")
        if let view = RelatedProductsVC as? RelatedProductsView {
            let interactor = RelatedProductsInteractor()
            let router = RelatedProductsRouter()
            let presenter = RelatedProductsPresenter(view: view, interactor: interactor, router: router, cartID: CartID)
            view.presenter = presenter
        }
        return RelatedProductsVC
    }
    
}
