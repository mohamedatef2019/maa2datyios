//
//  SuggestionVCPresenter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import Foundation

protocol SuggestionVCView: class {
    var presenter: SuggestionVCPresenter? { get set }
    func showIndicator()
    func dismissIndicator()
    func fetchDataSuccess()
    func showMsg(message: String, image: String)
}

class SuggestionVCPresenter{
    private weak var view: SuggestionVCView?
    private let interactor: SuggestionVCInteractor?
    
    init(view: SuggestionVCView, interactor: SuggestionVCInteractor) {
        self.view = view
        self.interactor = interactor
    }
    
    func validationOnText(suggestText: String){
        if suggestText.count > 0 && suggestText != "write..".localized {
            sendSuggestion(suggestText: suggestText)
        }else{
            self.view?.showMsg(message: "Please, Enter Your Suggest".localized, image: "warning")
        }
    }
    
    private func sendSuggestion(suggestText: String){
        let param = [
            "user_id":"\(SharedHandler.getUserID())",
            "jwt_token":SharedHandler.getUserJwt(),
            "comment":suggestText
        ]
        view?.showIndicator()
        interactor?.SuggestRequest(param: param, completionHandler: { (res, error) in
            if error == nil{
                if res?.status == 200{
                    self.view?.showMsg(message: res!.message, image: "like")
                }else{
                    self.view?.showMsg(message: res!.message, image: "warning")
                }
            }else{
                self.view?.showMsg(message: "Please, Try again later".localized, image: "warning")
            }
            self.view?.dismissIndicator()
        })
    }
}
