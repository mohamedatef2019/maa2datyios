//
//  SuggestionVC+SuggestViewDelegate.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

extension SuggestionVC: SuggestionVCView{
    
    func showIndicator() {
        indicator.startAnimating()
    }
    
    func dismissIndicator() {
        indicator.stopAnimating()
    }
    
    func fetchDataSuccess() {
        print("")
    }
    
    func showMsg(message: String, image: String) {
        SharedHandler.showMsg(message: message, image: image, ViewController: self)
    }

    
}
