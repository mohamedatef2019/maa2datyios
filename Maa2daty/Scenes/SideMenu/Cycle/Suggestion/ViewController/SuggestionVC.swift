//
//  SuggestionVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/18/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SuggestionVC: UIViewController {
    
    //MARK:- variable
    var presenter: SuggestionVCPresenter?
    
    
    //MARK:- Outlets
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    //MARK:- Actions
    @IBAction func suggestAcion(_ sender: Any) {
        presenter?.validationOnText(suggestText: textView.text)
    }
    
    //MARK:- apCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBTNs()
        let interactor = SuggestionVCInteractor()
        presenter = SuggestionVCPresenter(view: self, interactor: interactor)
        
        MenuRouter.CreateMenu(ViewController: self)
    }
}

