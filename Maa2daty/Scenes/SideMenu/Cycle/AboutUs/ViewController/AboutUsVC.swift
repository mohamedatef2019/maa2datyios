//
//  AboutUsVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/12/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
import WebKit

class AboutUsVC: UIViewController {

    @IBOutlet weak var webView_aboutUs: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBTNs()
        
        let request = URLRequest(url: URL(string: URLs.aboutUs)!)
        webView_aboutUs.load(request)
        
        MenuRouter.CreateMenu(ViewController: self)
    }
    

}
