//
//  MenuRouter.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import MOLH
import Foundation

class MenuRouter{
     class func CreateMenu(ViewController control: UIViewController){
        
        let sidemenu = UIBarButtonItem(image: UIImage(named: "Menu"),  style: .plain , target: self, action: Selector(("didTapEditButton:")))
        if control.revealViewController() != nil {
            sidemenu.target = control.revealViewController()
            
            if MOLHLanguage.currentAppleLanguage() == "en"{
                sidemenu.action = #selector(SWRevealViewController.revealToggle(_:))
            }
            else{
                sidemenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            }
            
            control.revealViewController().rearViewRevealWidth = UIScreen.main.bounds.width - 100.0
            control.revealViewController().frontViewController.view.addGestureRecognizer((control.revealViewController()?.panGestureRecognizer())!)
            control.revealViewController().frontViewController.view.addGestureRecognizer((control.revealViewController()?.tapGestureRecognizer())!)
            control.navigationItem.leftBarButtonItem = sidemenu
        }
    }
    
    class func swSegue(_ viewControllerId : String,_ viewController : UIViewController)  {
        let dvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewControllerId)
        let Rvc : SWRevealViewController = viewController.revealViewController() as SWRevealViewController
        Rvc.pushFrontViewController(dvc, animated: true)
    }
}
