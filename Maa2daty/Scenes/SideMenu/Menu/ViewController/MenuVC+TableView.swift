//
//  MenuVC+TableView.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

extension MenuVC: UITableViewDelegate, UITableViewDataSource{
    func setupTableView(){
        tbl_Menu.delegate = self
        tbl_Menu.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 16
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_Menu.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)")
        if indexPath.row == 0 {
            let HeaderCell = cell as! UserCell
            HeaderCell.settingData()
            return HeaderCell
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !SharedHandler.isLogged() && (indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 14){
            return 0
        }else if SharedHandler.isLogged() && indexPath.row == 13{
            return 0
        }else{
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            MenuRouter.swSegue("EditProfileVC", self)
            
        case 1:
            MenuRouter.swSegue("HomeVC", self)
            
        case 2:
            MenuRouter.swSegue("OffersVC", self)
        
        case 3:
            MenuRouter.swSegue("OrdersVC", self)
        
        case 4:
            MenuRouter.swSegue("FavouriteVC", self)
        
        case 5:
            MenuRouter.swSegue("NotificationVC", self)
            
        
        case 6:
            MenuRouter.swSegue("ShopsVC", self)
        
        
        case 7:
            MenuRouter.swSegue("ServicesSupporterVC", self)
            
        
        case 8:
            UserDefaults.standard.set(true, forKey: "isChange")
            MenuRouter.swSegue("HomeVC", self)
        
        case 9:
            shareLink(url: "https://play.google.com/store/apps/details?id=app.ma2edaty")
        
        case 10:
            MenuRouter.swSegue("SuggestionVC", self)
        
        case 11:
            MenuRouter.swSegue("TermsAndConditionsVC", self)
        
        case 12:
            MenuRouter.swSegue("AboutUsVC", self)
        
        case 13:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginCycle")
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
            MenuRouter.swSegue("HomeVC", self)
            
        case 14:
            LogoutRequest()
            UserDefaults.standard.removeObject(forKey: "isLogged")
            MenuRouter.swSegue("HomeVC", self)
            perform(#selector(removeUserDefaults), with: self, afterDelay: 0.5)
        
        case 15:
            UserDefaults.standard.set(true, forKey: "isGrand")
            MenuRouter.swSegue("HomeVC", self)
        default:
            print("")
        }
        
        tbl_Menu.deselectRow(at: indexPath, animated: true)
    }

    
    func LogoutRequest(){
        APIManager.sharedInstance.postRequestWithHeader(URLs.LogoutOut, header: ["lang":SharedHandler.currentLanguage()], Parameters: ["user_id":"\(SharedHandler.getUserID())","jwt_token":SharedHandler.getUserJwt()]) { (res) in
            
        }
    }
    
    @objc func removeUserDefaults(){
        SharedHandler.userLogout()
    }
}
