//
//  MenuVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tbl_Menu: UITableView!
    
    //MARK:- AppCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshView), name: NSNotification.Name(rawValue: "refresh"), object: nil)
    }

    @objc func refreshView(){
        self.tbl_Menu.reloadData()
    }
}
