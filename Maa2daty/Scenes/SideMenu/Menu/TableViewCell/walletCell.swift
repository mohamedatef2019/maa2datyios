//
//  walletCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class walletCell: UITableViewCell {

    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var switchForWallet: UISwitch!
    @IBOutlet weak var stackForPrice: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
