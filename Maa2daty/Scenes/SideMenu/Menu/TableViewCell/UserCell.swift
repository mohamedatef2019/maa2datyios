//
//  UserCell.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var img_Person: UIImageView!
    @IBOutlet weak var lbl_PersonName: UILabel!
    @IBOutlet weak var lbl_PersonMail: UILabel!

    override func awakeFromNib() {
        
    }
    func settingData(){
        lbl_PersonMail.text = SharedHandler.getEmail()
        lbl_PersonName.text = SharedHandler.getUserFirstName() + " " + SharedHandler.getUserLastName()
        setImage(url: SharedHandler.getuser_image(), picture: img_Person)
    }
}
