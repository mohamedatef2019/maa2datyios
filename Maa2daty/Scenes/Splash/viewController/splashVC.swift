//
//  splashVC.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/12/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import UIKit
//import MOLH

class splashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        perform(#selector(Landing), with: self, afterDelay: 0.5)
    }

    @objc func Landing(){
        pushLanding()
    }
}
