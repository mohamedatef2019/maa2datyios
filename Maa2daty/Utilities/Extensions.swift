//
//  Extensions.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/10/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import MOLH
import Kingfisher
import Foundation

extension UICollectionViewCell{
    func setImage(url: String, picture: UIImageView) {
        DispatchQueue.main.async {
            picture.kf.setImage(with: URL(string: url.encodeUrl())!)
        }
    }
}

extension UITableViewCell{
    func setImage(url: String, picture: UIImageView) {
        DispatchQueue.main.async {
            picture.kf.setImage(with: URL(string: url.encodeUrl())!)
        }
    }
}

extension String {
    
    func encodeUrl() -> String {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    var localized: String {
        // ar.lproj
        var lang = "en"
        if MOLHLanguage.isRTLLanguage(){
            lang = "ar"
        }
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
    
    func isValidUrl() -> Bool {
        let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: self)
        return result
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "^[0-9'@s]{8,13}$"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}

extension UIViewController{
    
    public func emptyLabel(_ view: UIView, msg: String){
        let w = UIScreen.main.bounds.width
        let h = UIScreen.main.bounds.height
        removeEmptyLabel(forView: view)
        let label = UILabel(frame: CGRect(x:0 , y: h / 2, width: w, height: 42))
        label.text = msg
        label.center = CGPoint(x: w / 2, y: h / 2)
        label.textAlignment = .center
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        label.tag = 9999
        view.addSubview(label)
    }
    
    public func removeEmptyLabel(forView view: UIView){
        if let label = view.viewWithTag(9999){
            label.removeFromSuperview()
        }
    }
    
    func pushLanding(){
        if MOLHLanguage.currentAppleLanguage() == "en"{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "swEN")
            UIApplication.shared.keyWindow?.rootViewController = vc
        }else{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "swAR")
            UIApplication.shared.keyWindow?.rootViewController = vc
        }
    }
    func setImage(url: String, picture: UIImageView) {
        DispatchQueue.main.async {
            picture.kf.setImage(with: URL(string: url.encodeUrl())!)
        }
    }
    
    func shareLink(url: String){
        let textShare = [ url ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    @objc func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    
    func addChild(id: String){
        let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id)
        self.addChild(popUpVC)
        popUpVC.view.frame = self.view.frame
        self.view.addSubview(popUpVC.view)
        popUpVC.didMove(toParent: self)
    }
    
    func navigationBTNs(){
        
        let cartBtn = UIBarButtonItem(image: UIImage(named: "cart (1)"), style: UIBarButtonItem.Style.done, target: self, action: #selector(cartPressed))
        let searchBtn = UIBarButtonItem(image: UIImage(named: "musica-searcher"), style: UIBarButtonItem.Style.done, target: self, action: #selector(searchPressed))
        
        if SharedHandler.isLogged(){
            self.navigationItem.rightBarButtonItems = [cartBtn, searchBtn]
        }else{
            self.navigationItem.rightBarButtonItems = [searchBtn]
        }
    }
    
    @objc func cartPressed(){
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CartVC")
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: false, completion: nil)
    }
    
    @objc func searchPressed(){
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchVC")
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: false, completion: nil)
    }
}
