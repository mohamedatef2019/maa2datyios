//
//  SharedHandler.swift
//  Maa2daty
//
//  Created by Madonna Makarious on 3/7/19.
//  Copyright © 2019 Madonna Makarious. All rights reserved.
//

import MOLH
import KMPopUp
import Foundation


class SharedHandler {
    
    static func isValidAction(textFeilds: [UITextField]) -> Bool {
        
        for myTextFeild in textFeilds {
            if !myTextFeild.hasText {
                return false
            }
        }
        return true
    }
    
    static func isGrand() -> Bool{
        let y = UserDefaults.standard.object(forKey: "isGrand") as? Bool ?? false
        return y
    }
    
    static func isChange() -> Bool{
        let y = UserDefaults.standard.object(forKey: "isChange") as? Bool ?? false
        return y
    }
    
    static func isLogout() -> Bool{
        let y = UserDefaults.standard.object(forKey: "isLogout") as? Bool ?? false
        return y
    }
    
    static func currentLanguage() -> String{
        let y = MOLHLanguage.currentAppleLanguage()
        return y
    }
    
    static func isLogged() -> Bool{
        let y = UserDefaults.standard.object(forKey: "isLogged") as? Bool ?? false
        return y
    }
    
    static func getUserID() -> Int{
        let y = UserDefaults.standard.object(forKey: "userID") as? Int ?? 5
        return y
    }
    
    static func getUserJwt() -> String{
        let y = UserDefaults.standard.object(forKey: "userJwt") as? String ?? "I3UFa9IaFc7RI7LlJvE2Iva0K"
        return y
    }
    
    static func getuserMob() -> String{
        let y = UserDefaults.standard.object(forKey: "userMob") as? String ?? ""
        return y
    }
    
    static func getDeviceToken() -> String{
        let y = UserDefaults.standard.object(forKey: "DeviceToken") as? String ?? "deviceToken"
        return y
    }
    
    static func getUserFirstName() -> String{
        let y = UserDefaults.standard.object(forKey: "FirstName") as? String ?? "first"
        return y
    }
    static func getUserLastName() -> String{
        let y = UserDefaults.standard.object(forKey: "LastName") as? String ?? "last"
        return y
    }
    static func getUserPhone() -> String{
        let y = UserDefaults.standard.object(forKey: "phone") as? String ?? "phone"
        return y
    }
    
    static func getEmail() -> String{
        let y = UserDefaults.standard.object(forKey: "email") as? String ?? "email"
        return y
    }
    
    static func getuser_image() -> String{
        let y = UserDefaults.standard.object(forKey: "user_image") as? String ?? "user_image"
        return y
    }
    
    static func getuser_status() -> String{
        let y = UserDefaults.standard.object(forKey: "user_status") as? String ?? "user_status"
        return y
    }
    static func getlocation() -> String{
        let y = UserDefaults.standard.object(forKey: "location") as? String ?? "location"
        return y
    }
    
    static func getlat() -> String{
        let y = UserDefaults.standard.object(forKey: "lat") as? String ?? "25.3548"
        return y
    }
    static func getlng() -> String{
        let y = UserDefaults.standard.object(forKey: "lng") as? String ?? "51.1839"
        return y
    }
    
    static func showMsg(message: String, image: String, ViewController: UIViewController){
        KMPopUp.ShowMessageWithDuration(ViewController, message: message, image: image)
    }
    
    static func showNoDataView( ViewController: UIViewController){
//        NoDataView
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NoDataView")
        ViewController.addChild(vc)
        vc.view.frame = ViewController.view.frame
        ViewController.view.addSubview(vc.view)
        vc.didMove(toParent: ViewController)
    }
    
    
    static func settingUserDefauls(userData: userData){
        let user = UserDefaults.standard
        
        user.set(userData.lat, forKey: "lat")
        user.set(userData.lng, forKey: "lng")
        user.set(userData.id, forKey: "userID")
        user.set(userData.phone, forKey: "phone")
        user.set(userData.email, forKey: "email")
        user.set(userData.location, forKey: "location")
        user.set(userData.jwt_token, forKey: "userJwt")
        user.set(userData.last_name, forKey: "LastName")
        user.set(userData.first_name, forKey: "FirstName")
        user.set(userData.user_image, forKey: "user_image")
        user.set(userData.user_status, forKey: "user_status")
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name("refresh"), object: self)
    }
    
    static func userLogout(){
        let user = UserDefaults.standard
        user.removeObject(forKey: "lat")
        user.removeObject(forKey: "lng")
        user.removeObject(forKey: "phone")
        user.removeObject(forKey: "email")
        user.removeObject(forKey: "userID")
        user.removeObject(forKey: "userJwt")
        user.removeObject(forKey: "isLogged")
        user.removeObject(forKey: "location")
        user.removeObject(forKey: "user_image")
        user.removeObject(forKey: "user_status")
        user.removeObject(forKey: "userFullName")
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name("refresh"), object: self)
    }
    
}
